# Literature Review

Dr. Paul Vecsei compiled an inventory of journal papers and agency publications focused on methods for capture-recapture estimates and alternative stock assessment approaches, covering primarily northern transboundary stocks but also including work from other areas. Abstracts and notes on relevance are included. 

Gottfried Pestal converted the inventory into a data-friendly format and built an interactive interface. 

## Interactive Interface

The interactive version of the reference library is hosted on [AIRTABLE](https://airtable.com/shrZMTV4KB4cNgSfG).  Here you can search, sort, and filter records based on key words, species, area, and author. It includes pdf versions of most reports and links to the original source where available.


## Simple File

[Vecsei2020_TakuSockeye_MarkRecapture_LitReview.csv](https://github.com/SOLV-Code/Taku-Sockeye-Public/blob/master/LitReview/Vecsei2020_TakuSockeye_MarkRecapture_LitReview.csv) is a single compact csv file with all the same information as the interactive reference library.  To download the file, left-click the link to open the file preview in the browser, then right-click the *Raw* button and select *"Save File As"*.

This file is also available as an excel file [Vecsei2020_TakuSockeye_MarkRecapture_LitReview.xlsx](https://github.com/SOLV-Code/Taku-Sockeye-Public/blob/master/LitReview/Vecsei2020_TakuSockeye_MarkRecapture_LitReview.xlsx). 







