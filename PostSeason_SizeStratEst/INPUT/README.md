# Input Files for Size-Stratified Estimates
Input files include tag releases and size data from the fish wheels, Canadian commercial catch data, and tag recoveries from the Canadian commercial fishery.

## Canadian_Taku_Sockeye_Commercial_Data.xlsx (catch.asl)
This file includes detailed sample information on Canadian commercial catches, but the only variable used is "Length - CAF." This file is used to determine the distribution of lengths in the catch data (rounded to the nearest 5 mm to match the cyi.asl.filtered data; fish wheel length data).

## fish_wheel_asl.csv (cyi.asl.filtered)
This file includes tag release information. The only variables used by the script is SPAGHETTI_TAG_NO and LENGTH_MILLIMETERS. The estimation script has a step of filtering out any records without size measurements, any records without a tag ID, duplicate tag IDs, any records from fish wheels other than 1 and 2, and any records from the side project (< 80,000 tag series). 

## catch_data_sockeye.csv (catch.data)
This file includes Canadian commercial catch by day. There is no length information in this file. The columns are: Year, Date, StatWeek, CdnCommCt.

## Lookup_LengthConversion.csv
This file includes an intercept and slope for converting cliethral arch to fork of tail (CAF) length to mid-eye to fork of tail (MEF) length by linear regression.  This conversion is necessary because the commercial catch sampling records are in CAF measurements, but the fish wheel sampling records are in MEF measurements.
Columns are: Label, Int, Slope, Comment.  

The source for the regression based on 1998 through 2002 data is Andel and Boyce (2004). The source for the regression based on 2019 to 2020 data is unpublished; see sheet MEF_CAF.xlsx data in the *INPUT* folder for the updated regression data.

## recovery_data_sockeye (catch.rec)
This file 
includes date and stat week for each tag recovered in the Canadian commercial fishery.
The columns are: Year, TagID, RecoveryDate, RecoveryStatWeek

## References:
[Andel and Boyce, 2004](https://www.psc.org/download/33/psc-technical-reports/2031/psctr14.pdf)  







