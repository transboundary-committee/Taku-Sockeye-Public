# BTSPAS ESTIMATE-LARGE
# Filter large data by split.use in 2b_SizeStratEst_KSTest
split.use # make sure this is correct value
size.plevel # make sure this is correct value
relrecap %>% 
  filter(LENGTH_MILLIMETERS >= split.use) -> relrecap_large

catch %>%
  mutate(CatchWithTags_Large = CatchWithTags * (1- size.plevel)) -> catch_large

# Full Week BTSPAS analysis
# Define the stratum variable as 1 = first stat week, 2=second stat week etc
fw.stratum.index <- data.frame(stratum.index=1:length(fw.stat.weeks),
                               stratum.label=as.character(fw.stat.weeks),
                               stringsAsFactors=FALSE)
fw.stratum.index

# get the data necessary for the call to BTSPAS
fw.data <- BTSPAS_input(relrecap_large, catch_large, "ReleaseStatWeek", "RecoveryStatWeek",
                        fw.stratum.index, catch.var="CatchWithTags_Large")

# check data to make sure that no recaptures before releases, i.e. below main diagonal
if(any(fw.data$m2.full[ outer(1:nrow(fw.data$m2.full), 1:ncol(fw.data$m2.full),">")] >0)){
  stop("Recoveries occuring before releases. Check the m2.full matrix in the output from BTSPAS_inpue")
}

# fit the BTSPAS model
fw.prefix <- paste("Postseason_Large_",sep="")

fit.BTSPAS(fw.data,prefix=fw.prefix, add.ones.at.start=FALSE, InitialSeed=sw.randomseed)

# fit the BTSPAS model with fall back 
fw.prefix.dropout <- paste("Postseason_Large_","fallback-",sep="")

fit.BTSPAS.dropout(fw.data,prefix=fw.prefix.dropout, n=tags.total.use, dropout=tags.dropped.use, add.ones.at.start=FALSE, InitialSeed=sw.randomseed) # see 1_Run_PooledEst for inputs

# Extract the directories with the fits
file.names <-dir()
file.names.fits<- file.names[grepl(paste("^Postseason_"), file.names)]
file.names.fits

prefix <- paste("Postseason_Large",sep="")

# Extract all of the estimates of the total run size
run.size <- plyr::ldply(file.names.fits, function(x){
  cat("Extracting total run size from ", x, "\n")
  load(file.path(x, "taku-fit-tspndenp-saved.Rdata"))
  Ntot <- taku.fit.tspndenp$summary["Ntot",]
  if(is.null(Ntot))Ntot <- rep(NA,9) # if model didn't converge properly
  #browser()
  Ntot <- as.data.frame(t(Ntot))
  Ntot[,1:7] <- round(Ntot[,1:7])
  Ntot$file=x
  Ntot
})
run.size
write.csv(run.size, file.path(data.directory.output, "/", paste(prefix,"runsize.csv",sep="")), row.names=TRUE)

# Extract the Petersen estimators
# Extract all of the estimates of the total run size
run.pet.size <- plyr::ldply(file.names.fits, function(x){
  cat("Extracting Petersen from ",x,"\n")
  load(file.path(x, "taku-fit-tspndenp-saved.Rdata"))
  Year <- as.numeric(substring(x, 2+regexpr('--',x,fixed=TRUE)))
  #browser()
  Ntot.pp <- taku.fit.tspndenp$PP$using.all.data
  if(is.null(Ntot.pp$N.se))Ntot.pp <- data.frame(N.est=NA, N.se=NA)
  # see if this included fall back
  Ntot.pp.fallback <- taku.fit.tspndenp$PP$using.all.data.fallback
  if(is.null(Ntot.pp.fallback$N.se))Ntot.pp.fallback <- data.frame(N.est=NA, N.se=NA)
  
  c(Ntot.pp.est=round(Ntot.pp$N.est), Ntot.pp.se=round(Ntot.pp$N.se), 
    Ntot.pp.fallback.est=round(Ntot.pp.fallback$N.est), Ntot.pp.fallback.se=round(Ntot.pp.fallback$N.se),
    file=x)
})
run.pet.size
write.csv(run.pet.size,file.path(data.directory.output, "/", paste(prefix,"_PP_runsize.csv",sep="")), row.names=TRUE)

# move files to correct directory
taku.prefix <- paste(fw.prefix,"-",year, sep="")
files_old <- paste0(getwd(), "/", taku.prefix)
files_new <- paste0(getwd(), "/" ,data.directory.output,"/", taku.prefix)
check.files <- dir(files_new, recursive=TRUE)
plyr::a_ply(check.files,1, function(x){if(file.exists(file.path(files_new,x))){file.remove(file.path(files_new,x))}})
file_move(files_old, files_new)

taku.prefix <- paste(fw.prefix.dropout,"-",year, sep="")
files_old <- paste0(getwd(), "/", taku.prefix)
files_new <- paste0(getwd(), "/" ,data.directory.output,"/", taku.prefix)
check.files <- dir(files_new, recursive=TRUE)
plyr::a_ply(check.files,1, function(x){if(file.exists(file.path(files_new,x))){file.remove(file.path(files_new,x))}})
file_move(files_old, files_new)
