# Post-Season Size-Stratified Estimates

This folder contains a script that reads functions, packages, and Taku sockeye capture-recapture data, then calculates size-stratified estimates. 

The calculation uses the SimplePetersenMod() function, which is a wrapper for the SimplePetersen() function from the [BTSPAS package](https://cran.r-project.org/web/packages/BTSPAS/index.html).

The sample is split into small fish and large fish, simple pooled Petersen estimates are calculated for each, and then combined.

Input files include tag releases and size data from the fish wheel, commercial catch data, and tag recoveries. Outputs include a csv file with estimates and a diagnostic plot. For details, refer to the *README* files in the  *INPUT* and *OUTPUT* folders. 

To run the post season estimate, the code needs to be run in the order: 

1_Run_PooledEst.R code  
2a_SizeStratEst_Plevels.R code  
2b_SizeStratEst_KSTest.R code  
3a_Run_BTSPAS_Est.R code
3b_Run_BTSPAS_Est_Large.R code
3c_Run_BTSPAS_Est_Small.R code
4_Plot_Ests.R code
5_ECDF_plots.R code  

even if only one part of the code is needed (e.g., you only want to run the K-S test).

The 2a_SizeStratEst_Plevels.R code is one way to determine the split between small and large fish (i.e., percentile of the size distribution of the catch as the splitting point betweem small and large fish), and the 2b_SizeStratEst_KSTest.R code is another way to determine the split between large and small fish (i.e., using the output of the Kolmogorov–Smirnov test (K-S test) using data from each year to determine the split). 

The K-S test is the "best" method of stratification using two size strata by splitting the capture-recapture data at the length where the K-S test statistic of length distribution of marks vs.length distribution of recaptures is maximized. This maximizes the difference in capture probability of the two length strata, which is what we are trying to accomplish with stratification. For example, if the K-S statistic (i.e., absolute difference in cumulative proportions of marks and of recaptures) is largest at 510 mm, then you would split the capture-recapture data at 509 mm and smaller for the small fish, and 510 and greater for the large fish, (i.e., make separate Petersen  estimates for each of the two, and then add them together). Variances would also be summed. However, if the greatest K-S statistic occurs at one of the extremes of the length distribution, then using the 30th percentile as the stratum break is acceptable (i.e., 2a_SizeStratEst_Plevels.R code).


In the 1_Run_PooledEst.R code, the inputs need to be updated based on the year's data and associated dropout rate:

```
year <- 2019  # this gets used in the outputs, but doesn't affect the calcs
# dropout inputs to use
tags.dropped.use <- 86  
tags.total.use <-  534 
```

Line 109 of the 2b_SizeStratEst_KSTest.R code displyes the MEF length split for large and small fish based on the K-S test.

The code 5_ECDF_plots.R was written to compare ECDF plots for two years together. The input files need to be changed 

```
read.csv('PostSeason_SizeStratEst/INPUT/2019/cyi.asl.filtered.csv') -> cyi.asl.filtered.2019 
read.csv('PostSeason_SizeStratEst/INPUT/2019/catch.rec.filtered.csv') -> catch.rec.filtered.2019 
read.csv('PostSeason_SizeStratEst/INPUT/2020/cyi.asl.filtered.csv') -> cyi.asl.filtered.2020 
read.csv('PostSeason_SizeStratEst/INPUT/2020/catch.rec.filtered.csv') -> catch.rec.filtered.2020
```
and the figure titles need to be updated accordingly. The output files for 5_ECDF_plots.R are here:
...Taku_sockeye_public\PostSeason_SizeStratEst.

Creating the excel output of the combined large and small BTSPAS estimates is not automated (i.e., to add the estimates and calculate the variance for the combined estimate). To do this, use the BTSPAS_Estimates_template.xlsx in the OUTPUT folder. Once the template is complete (i.e., the 3a_Run_BTSPAS_Est.R code, 3b_Run_BTSPAS_Est_Large.R code, and 3c_Run_BTSPAS_Est_Small.R code need to be run first), the BTSPAS figures can be created in the 4_Plot_Ests.R code.