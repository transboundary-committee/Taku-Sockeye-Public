# STEP-BY-STEP SCRIPT TO CALCULATE SIZE-STRATIFIED POOLED ESTIMATE
# code orginally written by Gottfried Pestal but adapted by Sara Miller (October 2020)
# FOR POST-SEASON REVIEW

# Step 1: GET FUNCTIONS ----

# Note: This script uses the SimplePetersenMod() custom function, which is
# a wrapper from the SimplePetersen() function from the BTSPAS package 
# (https://cran.r-project.org/web/packages/BTSPAS/index.html)

# load packages (need to be installed)
# load libraries
library(BTSPAS) 
library(lubridate)
library(fs)
library(rjags)
library(cellranger)
library(readxl)
library(tidyverse)
library(BTSPAS)
library(ggplot2)
library(readxl)
library(tibble)
library(plyr)
library(RCurl)
library("devtools")
devtools::install_github("commfish/fngr")
library(fngr)
extrafont::font_import()
windowsFonts(Times=windowsFont("TT Times New Roman"))
theme_set(theme_report(base_size = 10))

year <- 2020 # this gets used in the outputs, but doesn't affect the calcs
# dropout inputs to use
tags.dropped.use <- 69  # long-term "synthetic" avg = 13, 2019 value=  86; 2020 value = 69
tags.total.use <-  400 # long-term "synthetic" avg = 51, 2019 value= 534; 2020 value = 400

#set up data directories
data.directory.input <- file.path('PostSeason_SizeStratEst', 'INPUT', year)
data.directory.output <- file.path('PostSeason_SizeStratEst', 'OUTPUT',year)

# read in the local wrapper function
source("FUNCTIONS/FUNCTIONS_SimplePetersen&DropoutAdjustment.R")
source('FUNCTIONS/FUNCTIONS_BTSPAS_Wrappers.R')

# test the simple petersen function and its modified form
test.out <- SimplePetersenMod(n1 = 2454, m2 = 266, u2 = 4985.2)
# n1 Number of animals tagged and released
# m2 Number of animals from n1 that are recaptured. 
# u2 Number of unmarked animals in the second sample; n2 = u2 + m2

test.out
dropout.adj(Abd=test.out[,"est"],SE_Abd =test.out[,"se"] ,tags_dropped =24, tags_total = 118)

vec.test <- SimplePetersenMod(c(200,400,NA), c(10,20,40), c(300,600,900))
vec.test 

# Step 2: PREPARE THE INPUTS ----

# FOR NOW: ASSUMING THAT ALL RECORDS ARE VALID
# (i.e., not checking that recovery date is after release date etc.)

# read in asl data from the fish wheel
cyi.asl.raw <-read.csv(file.path(data.directory.input,"fish_wheel_asl.csv"),
         stringsAsFactors = FALSE) 

# filter out records
cyi.asl.raw %>% 
  distinct(SPAGHETTI_TAG_NO, .keep_all = TRUE) -> cyi.asl.filtered1

cyi.asl.raw %>%
  filter(SPAGHETTI_TAG_NO > 79999) -> cyi.asl.filtered2

cyi.asl.raw %>%
  filter(!is.na(SPAGHETTI_TAG_NO)) -> cyi.asl.filtered3

num.filtered1 <- dim(cyi.asl.raw)[1] - dim(cyi.asl.filtered1)[1]
num.filtered2 <- dim(cyi.asl.raw)[1] - dim(cyi.asl.filtered2)[1]
num.filtered3 <- dim(cyi.asl.raw)[1] - dim(cyi.asl.filtered3)[1]

cyi.asl.raw %>% 
  distinct(SPAGHETTI_TAG_NO, .keep_all = TRUE) %>% # keep unique tags
  select(SPAGHETTI_TAG_NO, LENGTH_MILLIMETERS, FW) %>%
  filter(SPAGHETTI_TAG_NO > 79999) %>%# delete tags from side project (This also takes care of na's in tag numbers)
  select(SPAGHETTI_TAG_NO, LENGTH_MILLIMETERS) %>% 
  mutate(SPAGHETTI_TAG_NO = as.integer(SPAGHETTI_TAG_NO))%>% 
  filter(!is.na(SPAGHETTI_TAG_NO)) -> cyi.asl.filtered # filter 'na' for tag SPAG TAG NEEDS TO BE AN INTEGER

num.filtered <- dim(cyi.asl.raw)[1] - dim(cyi.asl.filtered)[1]
print(paste(num.filtered,"out of",dim(cyi.asl.raw)[1],"filtered out duplicates, tags <80000, and missing tag numbers"))

cyi.asl.filtered$SPAGHETTI_TAG_NO[duplicated(cyi.asl.filtered$SPAGHETTI_TAG_NO)]  # remove duplicate tag numbers
length(unique(cyi.asl.filtered$SPAGHETTI_TAG_NO)) # total number of unique tags
sum(!is.na(cyi.asl.filtered$SPAGHETTI_TAG_NO)) # total number of tags

# read in catch and commercial asl data
catch.data <- read.csv(file.path(data.directory.input,"catch_data_sockeye.csv"),
                         stringsAsFactors = FALSE) 
catch.rec <- read.csv(file.path(data.directory.input,"recovery_data_sockeye.csv"),
                       stringsAsFactors = FALSE) 
catch.asl <-  read_excel(file.path(data.directory.input,"Canadian_Taku_Sockeye_Commercial_Data.xlsx"),
                         sheet=1,col_names=TRUE,na=c(""," ","NR")) 

# read in size CAF/MEF conversion file and select which version to use
sizeconv.pars <- read.csv(file.path(data.directory.input,"Lookup_LengthConversion.csv"),
                      stringsAsFactors = FALSE) %>%
                      dplyr::filter(Label == "1998_2002") # this selects the version

# convert size measurements from CAF to MEF using regression
catch.asl %>%
  select("Length - CAF") %>%
  select(length.caf = 1) %>% # rename variable
  mutate(length.mef = sizeconv.pars$Int + sizeconv.pars$Slope * length.caf) %>%
  filter(!is.na(length.mef)) -> catch.asl.filtered # delete rows with no lengths
write.csv(catch.asl.filtered, file.path(data.directory.input, "catch.asl.filtered.csv"), row.names = FALSE) # length representative of the catch 

# STEP 3: POOLED PETERSEN ESTIMATE ----
# storage object
pooled.test.df <- data.frame(Year = 1800, plevel=0,MEFsplit = 0, EstType = "Tmp", n1 = 0, 
                             m2 = 0,  u2 = 0 ,  est = 0,  se = 0, est.adj = 0,  se.adj = 0, stringsAsFactors=FALSE)


# Pooled Estimate ----
catch.rec %>% 
  distinct(TagID, .keep_all = TRUE) %>%  # keep unique tags
  select(TagID) %>%
  filter(!is.na(TagID)) %>%
  filter(TagID > 79999) %>% # delete tags from side project
  left_join(., cyi.asl.filtered, by = c("TagID" = "SPAGHETTI_TAG_NO")) -> catch.rec.filtered # add lengths from cyi asl by tag numbers
write.csv(catch.rec.filtered, file.path(data.directory.input, "catch.rec.filtered.csv"), row.names = FALSE) 

pooled.inputs <-  list(n1 = length(unique(cyi.asl.filtered$SPAGHETTI_TAG_NO)) , # number of unique tag ID in release file 
                       m2 = length(unique(catch.rec.filtered$TagID)), # number of unique tag ID in catch
                       u2 = sum(catch.data$CdnCommCt) - length(unique(catch.rec.filtered$TagID)))   # total CdnCommCt - tag recoveries

pooled.inputs

pooled.est<- SimplePetersenMod(n1 = pooled.inputs$n1, m2 = pooled.inputs$m2 , u2 = pooled.inputs$u2) 
pooled.est

pooled.est.adj <- dropout.adj(Abd=pooled.est[,"est"], # Pooled Petersen estimate
                              SE_Abd =pooled.est[,"se"] , # standard error of the estimate
                              tags_dropped = tags.dropped.use, 
                              tags_total = tags.total.use)

# Pooled Estimate Adjusted by Dropout
pooled.est.adj # dropout estimate including all tags (even ones with no lengths associated with them)

pooled.test.df <- rbind(pooled.test.df,
                        c(year, "None", "None", "All_data_PPE",pooled.est,round(pooled.est.adj[1]),
                            round(pooled.est.adj[2]))) # this includes missing lengths

# STEP 4: FILTER DATA TO ONLY INCLUDE TAGS WITH LENGTHS ASSOCIATED  ----
cyi.asl.filtered %>% 
  filter(!is.na(LENGTH_MILLIMETERS)) -> cyi.asl.filtered 
write.csv(cyi.asl.filtered, file.path(data.directory.input, "cyi.asl.filtered.csv"), row.names = FALSE) 

catch.rec.filtered %>% 
  filter(!is.na(LENGTH_MILLIMETERS)) -> catch.rec.filtered
write.csv(catch.rec.filtered, file.path(data.directory.input, "catch.rec.filtered.csv"), row.names = FALSE) 

catch.asl.filtered %>% 
  select(length.mef) %>%
  mutate(length.mef = round_any(length.mef,5)) -> length.mef # distribution of lengths in catch data (round to nearest 5 mm to match cyi asl data)

# STEP 5: POOLED PETERSEN ESTIMATE WITH DATA WITH NO LENGTHS ASSOCIATED  ----
pooled.inputs.length <-  list(n1 = length(unique(cyi.asl.filtered$SPAGHETTI_TAG_NO)) , # number of unique tag ID in release file 
                       m2 = length(unique(catch.rec.filtered$TagID)), # number of unique tag ID in catch
                       u2 = sum(catch.data$CdnCommCt) - length(unique(catch.rec.filtered$TagID)))   # total CdnCommCt - tag recoveries

pooled.inputs.length

pooled.est.length <- SimplePetersenMod(n1 = pooled.inputs.length$n1, m2 = pooled.inputs.length$m2 , u2 = pooled.inputs.length$u2) 
pooled.est.length

pooled.est.adj.length <- dropout.adj(Abd=pooled.est.length[,"est"],
                              SE_Abd =pooled.est.length[,"se"] ,
                              tags_dropped = tags.dropped.use, 
                              tags_total = tags.total.use)

pooled.est.adj.length # dropout estimate including all tags 

pooled.test.df <- rbind(pooled.test.df,
                        c(year, "None", "None", "PPE",pooled.est.length,round(pooled.est.adj.length[1]),
                          round(pooled.est.adj.length[2]))) # this includes missing lengths

pooled.test.df
