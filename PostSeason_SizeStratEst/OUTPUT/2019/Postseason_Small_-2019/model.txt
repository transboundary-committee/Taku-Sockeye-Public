

model {

# Time Stratified Petersen with NON Diagonal recapture and allowing for error in the smoothed U curve.
# Non-parametric estimateion of travel times for marked individuals.
#

#  Data input:
#      Nstrata.rel - number of strata where fish are releases
#      Nstrata.cap - number of (future strata) where fish are recaptured.
#      n1         - number of marked fish released
#      m2         - number of marked fish recaptured
#                   This is a matrix of size Nstrata.rel x (Nstrata.cap+1)
#                   with entries m2[i,j] = number of fish released in i and recaptured in j
#                   Entries in the last column are the number of fish NEVER recaptured from those
#                   released
#      u2         - number of unmarked fish captured (To be expanded to population).
#      logitP     - the recapture rates. Use NA if these are modelled, otherwise specify the logit(fixed value, e.g. -10 for 0).
#      Nfree.logitP - number of free logitP parameters
#      free.logitP.index - vector of length(Nfree.logitP) for the free logitP parameters
#      logitP.cov - covariates for logitP
#      NlogitP.cov  - number of logitP covariates
#      SplineDesign- spline design matrix of size [Nstrata, maxelement of n.b.notflat]
#                   This is set up prior to the call.
#      b.flat   - vector of strata indices where the prior for the b's will be flat.
#                 this is normally the first two of each spline segment
#      n.b.flat - number of b coefficients that have a flat prior
#      b.notflat- vector of strata indices where difference in coefficients is modelled
#      n.b.notflat- number of b coefficients that do not have a flat prior
#      tauU.alpha, tauU.beta - parameters for prior on tauU
#      taueU.alpha, taueU.beta - parameters for prior on taueU
#      prior.beta.logitP.mean, prior.beta.logitP.sd  - parameters for prior of coefficient of covariates for logitP
#      tauP.alpha, tauP.beta - parameter for prior on tauP (residual variance of logit(P)'s after adjusting for
#                         covariates)
#
#  Parameters of the model are:
#      p[i]
#       logitP[i]  = logit(p[i]) = logitP.cov*beta.logitP
#         The beta coefficients have a prior that is N(mean= prior.beta.logitP.mean, sd= prior.beta.logitP.sd)
#      U[i]
#       etaU[i]  = log(U[i])
#         which comes from spline with parameters bU[1... Knots+q]
#         + error term eU[i]
#
#      muTT[j] = mean(logit(delta[i,i+j-1])), j=1,...,Delta.max
#      sdTT = sd(logit(delta[i,i+j-1])), j=1,....,Delta.max
#      delta[i,i+j-1]=Theta[i,i+j-1]/(1-Theta[i,i]-...-Theta[i,i+j-2])
#

   ##### Fit the spline for the U's and specify hierarchial model for the logit(P)'s ######
   for(i in 1:(Nstrata.cap)){
        ## Model for U's
        logUne[i] <- inprod(SplineDesign[i,1:n.bU],bU[1:n.bU])  # spline design matrix * spline coeff
        etaU[i] ~ dnorm(logUne[i], taueU)T(,20)              # add random error but keep from getting too large
        eU[i] <- etaU[i] - logUne[i]
   }

   for(i in 1:Nfree.logitP){   # model the free capture rates using covariates
        mu.logitP[free.logitP.index[i]] <- inprod(logitP.cov[free.logitP.index[i],1:NlogitP.cov], beta.logitP[1:NlogitP.cov])
        ## logitP[free.logitP.index[i]] ~ dnorm(mu.logitP[free.logitP.index[i]],tauP)
        mu.epsilon[free.logitP.index[i]] <- mu.logitP[free.logitP.index[i]] - log(u2copy[free.logitP.index[i]] + 1) + etaU[free.logitP.index[i]]
        epsilon[free.logitP.index[i]] ~ dnorm(mu.epsilon[free.logitP.index[i]],tauP)

        logitP[free.logitP.index[i]] <- log(u2copy[free.logitP.index[i]] + 1) - etaU[free.logitP.index[i]] + epsilon[free.logitP.index[i]]
   }
   # define the last epsilon (including the extra needed for m2)
   for(i in Extra.strata.cap){
      epsilon[ Nstrata.cap + i] <- 0  # forces definition of epsilon1 ...epsilon[Nstrata.cap -> Extra.strata.cap]
   }

   ##### Priors and hyperpriors #####
   ## Transition probabilities -- continuation ratio model
   for(i in 1:Nstrata.rel){
     ## delta[i,j] is the probability that a marked fish released on day i passes the second trap
     ## on day i+j-1 given that it does not pass the on days i,...,i+j-2. r[i,j]=logit(delta[i,j])
     ## is assumed to have a normal distribution with mean muTT[j] and precision tauTT.
     r[i,1] ~ dnorm(muTT[1],tauTT)

     logit(Theta[i,1] ) <- r[i,1]

     for(j in 2:Delta.max){
       r[i,j] ~ dnorm(muTT[j],tauTT)
       logit(delta[i,j]) <- r[i,j]
       Theta[i,j] <- delta[i,j] * (1 - sum(Theta[i,1:(j-1)]))
     }
     Theta[i,Delta.max+1] <- 1- sum(Theta[i,1:Delta.max])
   }

#  derived parameters on actual movement probabilities
   logit(movep[1]) <- muTT[1]
   for(j in 2:Delta.max){
      movep[j] <- ilogit(muTT[j]) *(1- sum(movep[1:(j-1)]))
   }
   movep[Delta.max+1] <- 1- sum(movep[1:Delta.max])

#  prior on the movement rates. These are specified using the make.muTT.prior function
   for(j in 1:Delta.max){
     muTT[j] ~ dnorm(mean.muTT[j],tau.muTT[j])
   }
   tauTT~ dgamma(tauTT.alpha,tauTT.beta)
   sdTT <- 1/sqrt(tauTT)

   ## Run size - flat priors
   for(i in 1:n.b.flat){
      bU[b.flat[i]] ~ dnorm(0, 1E-6)
   }

   ## Run size - priors on the difference
   for(i in 1:n.b.notflat){
      xiU[b.notflat[i]] <- 2*bU[b.notflat[i]-1] - bU[b.notflat[i]-2]
      bU [b.notflat[i]] ~ dnorm(xiU[b.notflat[i]],tauU)
   }

   tauU ~ dgamma(tauU.alpha,tauU.beta)  # Notice reduction from .0005 (in thesis) to .05
   sigmaU <- 1/sqrt(tauU)
   taueU ~ dgamma(taueU.alpha,taueU.beta) # dgamma(100,.05) # Notice reduction from .0005 (in thesis) to .05
   sigmaeU <- 1/sqrt(taueU)

   ## Capture probabilities covariates
   for(i in 1:NlogitP.cov){
      beta.logitP[i] ~ dnorm(prior.beta.logitP.mean[i], 1/prior.beta.logitP.sd[i]^2)  # rest of beta terms are normal 0 and a large variance
   }
   beta.logitP[NlogitP.cov+1] ~ dnorm(0, .01) # dummy so that covariates of length 1 function properly
   tauP ~ dgamma(tauP.alpha,tauP.beta)
   sigmaP <- 1/sqrt(tauP)

   ##### Likelihood contributions #####
   ## marked fish ##
   for(i in 1:Nstrata.rel){
      # Compute cell probabilities
      for(j in 1:(Delta.max+1)){
        Pmarked[i,j] <- Theta[i,j] * p[i+j-1]
      }
      Pmarked[i,Delta.max+2] <- 1- sum(Pmarked[i,1:(Delta.max+1)])

      # Likelihood contribution
      m2[i,1:(Delta.max+2)] ~ dmulti(Pmarked[i,],n1[i])
   }

   ## Capture probabilities and run size
   for(j in 1:(Nstrata.cap + Extra.strata.cap)){
      logit(p[j]) <- logitP[j]       # convert from logit scale
   }
   for(j in 1:Nstrata.cap){
      U[j]   <- round(exp(etaU[j]))       # convert from log scale
      u2[j] ~ dbin(p[j],U[j])      # capture of newly unmarked fish
   }

   ##### Derived Parameters #####
   Utot <- sum( U[1:Nstrata.cap])          # Total number of unmarked fish
   Ntot <- sum(n1[1:Nstrata.rel]) + Utot  # Total population size including those fish marked and released
} # end of model

