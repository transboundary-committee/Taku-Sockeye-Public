# Canyon Island fish wheel CPUE
# author: Sara E Miller 
# contact: sara.miller@alaska.gov; 907-465-4245
# Last edited: April 2020
# This code creates the derived cpue data (output/CYI_cpue_FW123.csv or CYI_cpue_FW12.csv). 
# The files output/CYI_cpue_FW123_expanded.csv and CYI_catch_FW123_expanded.csv are the transposed catch and effort
# data columns from output/CYI_cpue_FW123.csv.

# For 2016 and 2017, fish wheel 1, fish wheel 2, and fish wheel 3 CPUE are included in the FW123 file. 
# Catch data in the ASL database (ADF&G database OceanAK) includes catch from all three FWs and cannot be separated.

# The cumulative CPUE data averaged over a 5, 10, or 20 year period should contain all three fish wheels (file: output/CYI_cpue_FW123.csv).

# load libraries----
devtools::install_github("commfish/fngr")
library(fngr)
library(ggplot2)
library(extrafont)
library(tidyverse)
library(dgof)
library(readxl)
library(Matching)
library(devtools)
library(cowplot)
library(lubridate)
library(scales)
library(zoo)

# set graphics----
theme_sleek <- function(base_size = 12, base_family = "Arial") {
  half_line <- base_size/2
  theme_light(base_size = 12, base_family = "Arial") +
    theme(
      panel.grid.major = element_blank(),
      panel.grid.minor = element_blank(),
      axis.ticks.length = unit(half_line / 2.2, "pt"),
      strip.background = element_rect(fill = NA, colour = NA),
      strip.text.x = element_text(colour = "black"),
      strip.text.y = element_text(colour = "black"),
      panel.border = element_rect(fill = NA),
      legend.key.size = unit(0.9, "lines"),
      legend.key = element_rect(colour = NA, fill = NA),
      legend.background = element_rect(colour = NA, fill = NA)
    )
}
windowsFonts(Times=windowsFont("Times New Roman"))
theme_set(theme_sleek())

# CPUE fishwheel 1, fishwheel 2, and fishwheel 3----
# data ----
read.csv('DATA/BaseData/Large_Files/Taku_Sockeye_CYI_TagReleases.csv') -> catch #catch includes fish from all three fish #FW3 only run in 2016 and 2017
read.csv('fishwheel_cpue_archived/data/Taku_Sockeye_CYI_Effort_FW1and2.csv') -> effort_FW1_FW2 
read.csv('fishwheel_cpue_archived/data/Taku_Sockeye_CYI_Effort_FW3.csv') -> effort_FW3 
read.csv('fishwheel_cpue_archived/data/Taku_Sockeye_CYI_catch_FW3.csv') -> catch_FW3

# catch and cumsum data by year and date
catch %>% 
  mutate(date = mdy_hm(sample_date))%>% 
  replace_na(list(value = 0)) %>%
  mutate(julian = yday(date))%>%
  dplyr::select(year, julian, project_cc) %>%
  count(year, julian) %>%
  mutate(year = as.factor(year))  %>% 
  group_by(year) %>%        
  mutate(cumsum_catch = cumsum(n),
  catch=n) %>% 
  dplyr::select(year, julian, catch, cumsum_catch) -> catch

# create effort data for fish wheel 1 and fish wheel 2
effort_FW1_FW2  %>%
  mutate(date = mdy(date))%>% 
  gather(year, value, -Day, -date, -DayNum, -Month) %>%   
  mutate(year = as.numeric(substring(year,2,5)))  %>% 
  replace_na(list(value = 0)) %>% 
  group_by(year) %>% 
  mutate(effort12 = value) %>% 
  dplyr::select(year, date, effort12)%>%
  mutate(month = month(date),
         day=day(date)) -> effort12
effort12$date2 <- paste(effort12$year, effort12$month, effort12$day, sep="-") %>% ymd() %>% as.Date()
effort12 %>% 
  mutate(julian = yday(date2))%>%
  dplyr::select(year, julian, effort12) -> effort12
  
# create effort data for fish wheel 3
effort_FW3  %>%
  mutate(date = mdy(date))%>% 
  gather(year, value, -Day, -date, -DayNum, -Month) %>%   
  mutate(year = as.numeric(substring(year,2,5)))  %>% 
  replace_na(list(value = 0)) %>% 
  group_by(year) %>% 
  mutate(effort3 = value) %>% 
  dplyr::select(year, date, effort3)%>%
  mutate(month = month(date),
         day=day(date)) -> effort3
effort3$date2 <- paste(effort3$year, effort3$month, effort3$day, sep="-") %>% ymd() %>% as.Date()
effort3 %>% 
  mutate(julian = yday(date2))%>%
  dplyr::select(year, julian, effort3) -> effort3

# merge fish wheel 1 and fish wheel 2 with fish wheel 3 effort data
effort<- merge(x= effort12, y= effort3, by=c("year","julian"), all= T)
effort[is.na(effort)] <- 0
effort %>%
  mutate(effort = as.numeric(effort12+effort3))%>%
  dplyr::select(year, julian, effort) -> effort #effort data for FW1, FW2, and FW3 combined for years 2016 and 2017

# merge effort data with catch data
cpue_data<- merge(x= effort, y= catch, by=c("year","julian"), all= T)

# daily and cum cpue
cpue_data %>%
  filter (year>1984) %>%
  mutate(daily_cpue = ifelse(effort>0, catch/effort,NA)) %>% #daily cpue
  group_by(year) %>% 
  mutate(cumsum_cpue=cumsum(replace_na(daily_cpue, 0))) %>% #cum cpue
  group_by(year) %>% 
  mutate(daily_prop_cpue = daily_cpue/max(cumsum_cpue)) %>%  #daily prop cpue
  group_by(year) %>% 
  mutate(cumsum_prop_cpue=cumsum(replace_na(daily_prop_cpue, 0)))-> cpue_data #cum prop cpue

# rolling average (5, 10, 20 years)
cpue_data %>%
  group_by(julian) %>%
  mutate(cpue_data_5yr =rollapply(cumsum_prop_cpue,5,mean,align='right',fill=NA),
         cpue_data_10yr =rollapply(cumsum_prop_cpue,10,mean,align='right',fill=NA),
         cpue_data_20yr =rollapply(cumsum_prop_cpue,20,mean,align='right',fill=NA)) %>%
  mutate(data = "FW1_2_3")-> cpue_data_FW123
write.csv(cpue_data_FW123, "fishwheel_cpue_archived/output/CYI_cpue_FW123.csv") #FW1-FW2 cpue data combined

# CPUE fish wheel 1 and fish wheel 2----
# data ----
read.csv('DATA/BaseData/Large_Files/Taku_Sockeye_CYI_TagReleases.csv') -> catch  #catch includes fish from all three FWS #FW3 only run in 2016 and 2017
read.csv('fishwheel_cpue_archived/data/Taku_Sockeye_CYI_Effort_FW1and2.csv') -> effort_FW1_FW2 
read.csv('fishwheel_cpue_archived/data/Taku_Sockeye_CYI_catch_FW3.csv') -> catch_FW3

# catch by year and date for fish wheel 1, fish wheel 2, and fish wheel 3
catch %>% 
  mutate(date = mdy_hm(sample_date))%>% 
  replace_na(list(value = 0)) %>%
  mutate(julian = yday(date))%>%
  dplyr::select(year, julian, project_cc) %>%
  count(year, julian) %>%
  mutate(year = as.factor(year))  %>% 
  group_by(year) %>%        
  mutate(catch_FW123=n) %>% 
  dplyr::select(year, julian, catch_FW123) -> catch_FW123

# create catch data for fish wheel 3
catch_FW3  %>%
  mutate(date = mdy(date))%>% 
  gather(year, value, -date) %>%   
  mutate(year = as.numeric(substring(year,2,5)))  %>% 
  replace_na(list(value = 0)) %>% 
  group_by(year) %>% 
  mutate(catch_FW3 = value) %>% 
  dplyr::select(year, date, catch_FW3)%>%
  mutate(month = month(date),
         day=day(date)) -> catch_FW3
catch_FW3$date2 <- paste(catch_FW3$year, catch_FW3$month, catch_FW3$day, sep="-") %>% ymd() %>% as.Date()
catch_FW3 %>% 
  mutate(julian = yday(date2))%>%
  dplyr::select(year, julian, catch_FW3) -> catch_FW3

# merge catch data
catch_data<- merge(x= catch_FW3, y= catch_FW123, by=c("year","julian"), all = T)

# extract catch from fish wheel 1 and fish wheel 2 by subtracting catch from fish wheel 3 from catch from fish wheel 1 and fish wheel 2
catch_data  %>%
  replace_na(list(catch_FW3 = 0)) %>% 
  replace_na(list(catch_FW123 = 0)) %>% 
  mutate(catch_FW12 = catch_FW123-catch_FW3) %>% 
  dplyr::select(year, julian, catch_FW12) -> catch_FW12

catch_FW12 %>%
  replace_na(list(catch_FW12 = 0)) %>%
  group_by(year) %>%        
  mutate(cumsum_catch = cumsum(catch_FW12))-> catch_FW12

# create effort data for fish wheel 1 and fish wheel 2
effort_FW1_FW2  %>%
  mutate(date = mdy(date))%>% 
  gather(year, value, -Day, -date, -DayNum, -Month) %>%   
  mutate(year = as.numeric(substring(year,2,5)))  %>% 
  replace_na(list(value = 0)) %>% 
  group_by(year) %>% 
  mutate(effort12 = value) %>% 
  dplyr::select(year, date, effort12)%>%
  mutate(month = month(date),
         day=day(date)) -> effort12
effort12$date2 <- paste(effort12$year, effort12$month, effort12$day, sep="-") %>% ymd() %>% as.Date()
effort12 %>% 
  mutate(julian = yday(date2))%>%
  dplyr::select(year, julian, effort12) -> effort12

# merge effort data with catch data
cpue_data<- merge(x = effort12, y = catch_FW12, by=c("year","julian"), all= T)

# daily and cumulative cpue
cpue_data %>%
  filter (year>1984) %>%
  mutate(daily_cpue = ifelse(effort12>0, catch_FW12/effort12,NA)) %>% #daily cpue
  group_by(year) %>% 
  mutate(cumsum_cpue=cumsum(replace_na(daily_cpue, 0))) %>% #cum cpue
  group_by(year) %>% 
  mutate(daily_prop_cpue = daily_cpue/max(cumsum_cpue)) %>%  #daily prop cpue
  group_by(year) %>% 
  mutate(cumsum_prop_cpue=cumsum(replace_na(daily_prop_cpue, 0)))-> cpue_data #cum prop cpue

# rolling average for fish wheel 1 and fish wheel 2 (5, 10, 20 years)
cpue_data %>%
  group_by(julian) %>%
  mutate(cpue_data_5yr=rollapply(cumsum_prop_cpue,5,mean,align='right',fill=NA),
         cpue_data_10yr=rollapply(cumsum_prop_cpue,10,mean,align='right',fill=NA),
         cpue_data_20yr=rollapply(cumsum_prop_cpue,20,mean,align='right',fill=NA)) %>%
  mutate(data = "FW1_2")-> cpue_data_FW12
write.csv(cpue_data_FW12, "fishwheel_cpue_archived/output/CYI_cpue_FW12.csv") 

# create cumulative percent fishwheel cpue figure for 20, 10, and 5 year average
cpue_data_FW12 %>%
  filter(year == max(year)) -> x
cpue_data_FW123 %>%
  filter(year == max(year)) -> y 
fig_data <- rbind(x, y)

ggplot() + 
  geom_line(data = fig_data, aes(x = julian, y = cpue_data_20yr, color = data), size=1.5, lty=1) +
  labs(x = '', y = '') +
  scale_color_manual(values = c('FW1_2_3' = 'grey50',
                                'FW1_2' = 'black')) +
  annotate(geom="text",x=160,
           y=1,label="a) 20 Year Average",fontface="bold")+
 theme(legend.justification=c(1,0), legend.position=c(0.9,0), legend.title=element_blank()) -> plot1
 
ggplot() + 
  geom_line(data = fig_data, aes(x = julian, y = cpue_data_10yr, color = data), size=1.5, lty=1) +
  labs(x = '', y = 'Cumulative Sockeye CYI FW CPUE') +
  scale_color_manual(values = c('FW1_2_3' = 'grey50',
                                'FW1_2' = 'black')) +
  annotate(geom="text",x=160,
           y=1,label="b) 10 Year Average",fontface="bold")+
  theme(legend.justification=c(1,0), legend.position=c(0.9,0), legend.title=element_blank()) -> plot2

ggplot() + 
  geom_line(data = fig_data, aes(x = julian, y = cpue_data_5yr, color = data), size=1.5, lty=1) +
  labs(x = '', y = '') +
  scale_color_manual(values = c('FW1_2_3' = 'grey50',
                                'FW1_2' = 'black')) +
  annotate(geom="text",x=160,
           y=1,label="c) 5 Year Average",fontface="bold")+
  theme(legend.justification=c(1,0), legend.position=c(0.9,0), legend.title=element_blank()) -> plot3

cowplot::plot_grid(plot1, plot2, plot3,  align = "v", nrow = 3, ncol=1) 
ggsave("fishwheel_cpue_archived/figs/cumsum.png", dpi = 100, height = 8, width = 6, units = "in")

# create cumulative percent fishwheel cpue figure for 5, 10, and 20 year average for fish wheel 1 and fish wheel 2
cpue_data_FW12 %>%
  filter(year == max(year)) -> fig_data1
cpue_data_FW123 %>%
  filter(year == max(year)) -> fig_data2

ggplot() + 
  geom_line(data = fig_data1, aes(x = julian, y = cpue_data_20yr), size=1.5, lty=1, col= 'grey80') +
  geom_line(data = fig_data1, aes(x = julian, y = cpue_data_10yr), size=1.5, lty=1, col ="black") +
  geom_line(data = fig_data1, aes(x = julian, y = cpue_data_5yr), size=1.5, lty=3, colora = 'grey90') +
  labs(x = '', y = 'Cumulative Sockeye CYI FW CPUE') +
  annotate(geom="text",x=165,
           y=1,label="a) Fish wheels 1 and 2",fontface="bold")+
  theme(legend.justification=c(1,0), legend.position=c(0.8,0), legend.title=element_blank()) -> plot1

ggplot() + 
  geom_line(data = fig_data2, aes(x = julian, y = cpue_data_20yr), size=1.5, lty=1, col= 'grey80') +
  geom_line(data = fig_data2, aes(x = julian, y = cpue_data_10yr), size=1.5, lty=1, col ="black") +
  geom_line(data = fig_data2, aes(x = julian, y = cpue_data_5yr), size=1.5, lty=3, colora = 'grey90') +
  labs(x = '', y = 'Cumulative Sockeye CYI FW CPUE') +
  annotate(geom="text",x=170,
           y=1,label="b) Fish wheels 1, 2, and 3",fontface="bold")+
  theme(legend.justification=c(1,0), legend.position=c(0.9,0), legend.title=element_blank()) -> plot2

cowplot::plot_grid(plot1, plot2,  align = "h", nrow = 2, ncol=1) 
ggsave("fishwheel_cpue_archived/figs/cumsum_average.png", dpi = 100, height = 6, width = 6, units = "in")

# CPUE 2016 and 2017----
cpue_data_FW12 %>%
  filter (year %in% c(2016, 2017)) %>% 
  dplyr::select(year, julian, catch_FW12, effort12) -> cpue_data_FW12

read.csv('fishwheel_cpue_archived/data/Taku_Sockeye_CYI_Effort_FW3.csv') -> effort_FW3 
read.csv('fishwheel_cpue_archived/data/Taku_Sockeye_CYI_catch_FW3.csv') -> catch_FW3

# create catch data for fish wheel 3
catch_FW3  %>%
  mutate(date = mdy(date))%>% 
  gather(year, value, -date) %>%   
  mutate(year = as.numeric(substring(year,2,5)))  %>% 
  replace_na(list(value = 0)) %>% 
  group_by(year) %>% 
  mutate(catch_FW3 = value) %>% 
  dplyr::select(year, date, catch_FW3)%>%
  mutate(month = month(date),
         day=day(date)) -> catch_FW3
catch_FW3$date2 <- paste(catch_FW3$year, catch_FW3$month, catch_FW3$day, sep="-") %>% ymd() %>% as.Date()
catch_FW3 %>% 
  mutate(julian = yday(date2))%>%
  dplyr::select(year, julian, catch_FW3) -> catch_FW3

# create effort data for fishwheel 3
effort_FW3  %>%
  mutate(date = mdy(date))%>% 
  gather(year, value, -Day, -date, -DayNum, -Month) %>%   
  mutate(year = as.numeric(substring(year,2,5)))  %>% 
  replace_na(list(value = 0)) %>% 
  group_by(year) %>% 
  mutate(effort3 = value) %>% 
  dplyr::select(year, date, effort3)%>%
  mutate(month = month(date),
         day=day(date)) -> effort3
effort3$date2 <- paste(effort3$year, effort3$month, effort3$day, sep="-") %>% ymd() %>% as.Date()
effort3 %>% 
  mutate(julian = yday(date2))%>%
  dplyr::select(year, julian, effort3) -> effort3

# merge effort data with catch data
cpue_data_FW3 <- merge(x= effort3, y= catch_FW3, by=c("year","julian"), all= T)

# 2016 data fishwheel 1 and fish wheel 2
cpue_data_FW12 %>%
  filter (year %in% c(2016)) %>% 
  filter (julian %in% c(159:223)) -> cpue_data_FW12_2016

cpue_data_FW12_2016 %>%
  mutate(daily_cpue_FW12 = ifelse(effort12>0, catch_FW12/effort12,NA)) %>% #daily cpue
  group_by(year) %>% 
  mutate(cumsum_cpue=cumsum(replace_na(daily_cpue_FW12, 0))) %>% #cum cpue
  group_by(year) %>% 
  mutate(daily_prop_cpue = daily_cpue_FW12/max(cumsum_cpue)) %>%  #daily prop cpue
  group_by(year) %>% 
  mutate(cumsum_prop_cpue_FW12=cumsum(replace_na(daily_prop_cpue, 0)))-> cpue_data_FW12_2016 #cum prop cpue

# 2016 data fish wheel 3
cpue_data_FW3 %>%
  filter (year %in% c(2016)) %>% 
  filter (julian %in% c(159:223)) -> cpue_data_FW3_2016

cpue_data_FW3_2016 %>%
  mutate(daily_cpue_FW3 = ifelse(effort3>0, catch_FW3/effort3,NA)) %>% #daily cpue
  group_by(year) %>% 
  mutate(cumsum_cpue=cumsum(replace_na(daily_cpue_FW3, 0))) %>% #cum cpue
  group_by(year) %>% 
  mutate(daily_prop_cpue = daily_cpue_FW3/max(cumsum_cpue)) %>%  #daily prop cpue
  group_by(year) %>% 
  mutate(cumsum_prop_cpue_FW3=cumsum(replace_na(daily_prop_cpue, 0)))-> cpue_data_FW3_2016 #cum prop cpue

# 2017 data fishwheel 1 and fish wheel 2
cpue_data_FW12 %>%
  filter (year %in% c(2017)) %>% 
  filter (julian %in% c(171:249)) -> cpue_data_FW12_2017

cpue_data_FW12_2017 %>%
  mutate(daily_cpue_FW12 = ifelse(effort12>0, catch_FW12/effort12,NA)) %>% #daily cpue
  group_by(year) %>% 
  mutate(cumsum_cpue=cumsum(replace_na(daily_cpue_FW12, 0))) %>% #cum cpue
  group_by(year) %>% 
  mutate(daily_prop_cpue = daily_cpue_FW12/max(cumsum_cpue)) %>%  #daily prop cpue
  group_by(year) %>% 
  mutate(cumsum_prop_cpue_FW12=cumsum(replace_na(daily_prop_cpue, 0)))-> cpue_data_FW12_2017 #cum prop cpue

#2017 data fish wheel 3
cpue_data_FW3%>%
  filter (year %in% c(2017)) %>% 
  filter (julian %in% c(171:249)) -> cpue_data_FW3_2017

cpue_data_FW3_2017 %>%
  mutate(daily_cpue_FW3 = ifelse(effort3>0, catch_FW3/effort3,NA)) %>% #daily cpue
  group_by(year) %>% 
  mutate(cumsum_cpue=cumsum(replace_na(daily_cpue_FW3, 0))) %>% #cum cpue
  group_by(year) %>% 
  mutate(daily_prop_cpue = daily_cpue_FW3/max(cumsum_cpue)) %>%  #daily prop cpue
  group_by(year) %>% 
  mutate(cumsum_prop_cpue_FW3=cumsum(replace_na(daily_prop_cpue, 0)))-> cpue_data_FW3_2017 #cum prop cpue

# merge datasets
cpue_data_2016<- merge(x= cpue_data_FW12_2016, y= cpue_data_FW3_2016, by=c("year","julian"), all= T)
cpue_data_2017<- merge(x= cpue_data_FW12_2017, y= cpue_data_FW3_2017, by=c("year","julian"), all= T)

#create cumulative percent fish wheel cpue figure for fish wheel 1 and 2, and for fish wheel 3 data for 2016 and 2017
ggplot() + 
  geom_line(data=cpue_data_2016, aes(x = julian, y = cumsum_prop_cpue_FW12), size=2) +  
  geom_line(data=cpue_data_2016, aes(x = julian, y = cumsum_prop_cpue_FW3), lty=2) + 
  labs(x = 'Julian day', y = 'Cumulative Sockeye CYI FW CPUE') +
  annotate(geom="text",x=163, y=1,label="a) 2016",fontface="bold") +theme(legend.justification=c(1,0), legend.position=c(0.9,0),
                                                       legend.title=element_blank())-> plot1

ggplot() + 
  geom_line(data=cpue_data_2017, aes(x = julian, y = cumsum_prop_cpue_FW12), size=2) +  
  geom_line(data=cpue_data_2017, aes(x = julian, y = cumsum_prop_cpue_FW3), lty=2) + 
  labs(x = 'Julian day', y = 'Cumulative Sockeye CYI FW CPUE') +
  annotate(geom="text",x=175, y=1,label="b) 2017",fontface="bold") +theme(legend.justification=c(1,0), legend.position=c(0.9,0),
                                                                          legend.title=element_blank())-> plot2

cowplot::plot_grid(plot1, plot2,   align = "h", nrow = 2, ncol=1) 
ggsave("fishwheel_cpue_archived/figs/cumsum_by_year.png", dpi = 100, height = 8, width = 6, units = "in")

# transformed dataset
read.csv('fishwheel_cpue_archived/output/CYI_cpue_FW123.csv') -> cpue
cpue %>% 
  dplyr::select(year, julian, catch) %>%
  spread(year, catch) %>%  
  write.csv(., "fishwheel_cpue_archived/output/CYI_catch_FW123_expanded.csv") #catch
cpue %>% 
  dplyr::select(year, julian, daily_prop_cpue) %>%
  spread(year, daily_prop_cpue) %>%   
  write.csv(., "fishwheel_cpue_archived/output/CYI_cpue_FW123_expanded.csv") #daily prop cpue