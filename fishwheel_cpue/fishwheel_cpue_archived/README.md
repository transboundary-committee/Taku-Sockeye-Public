# Canyon Island CPUE by Fish wheel
## Background
The objective of this analysis was to determine if catch and effort data from fish wheel three should be excluded from the runtiming curve (for years 2016 and 2017) if the curve is averaged over a 5, 10, or 20 year period. The runtiming curve is used inseason and posteason for expansions of abundance estimates (e.g. early fish wheel removal).



**NOTE**: You need to extract the zipped data files before running this code. Place the extracted data files in the location: fishwheel_cpue_archived/data.


The data sources used in this analysis are:

1. Taku_Sockeye_CYI_TagReleases.csv
   - Catch data from the fish wheels at Canyon Island downloaded from the ASL database (ADF&G database OceanAK). This includes catch from all three fish wheels by day and cannot be separated. In 2016 and 2017, there were three fish wheels. In all other years, there were only two fish wheels.  
2. Taku_Sockeye_CYI_Effort_FW1and2.csv 
   - Effort data for fish wheel one and fish wheel two.
     - This data can be found in the inseason datasheets. Effort data is the total number of hours the fish wheels were run each day.
3. Taku_Sockeye_CYI_Effort_FW3.csv 
   - Effort data for fish wheel three (years 2016 and 2017 only).
     - This data can be found in the inseason datasheets. Effort data is the total number of hours fish wheel three was run each day.
4. Taku_Sockeye_CYI_catch_FW3.csv 
   - Catch data for fish wheel three (years 2016 and 2017 only.
     - This data can be found in the inseason datasheets. This is the total catch by day from fish wheel three only. 

## Methods
Data for the analysis to determine if fish wheel three should be excluded is from years 1985 through 2018. Catch data in the OceanAK database (i.e. ASL data) from 1985 through 2015 and 2018 only contains catch from fish wheels one and two, while catch data from years 2016 and 2017 includes catch from fish wheels one, two, and three combined. Effort data (number of hours the fish wheels were run per day) is from inseason datasheets and is separated by fish wheel. Catch data from fish wheel three is only available from inseason datasheets for years 2016 and 2017. Therefore, an analysis was done to determine if catch and effort data from fish wheel three should be excluded from the runtiming curve (for years 2016 and 2017) if the curve is averaged over a 5, 10, or 20 year period.

To determine if there was a difference in cumulative proportional CPUE by fish wheel, cumulative proportional CPUE  was calculated for fish wheels one and two combined and for fish wheel three separately for years 2016 and 2017. This data was standardized by date (i.e., 2016 dates were 7 June to 10 August and 2017 dates were 20 June to 6 September) because fish wheel three had a truncated season compared to fishwheel one and fishwheel two. Then the average cumulative proportional CPUE was calculated for 5, 10, and 20 year averages for fish wheel one and two combined, and then for fish wheel one, two, and three combined for years 1985 through 2018. Analysis of data for 1985 through 2018 included dates 27 May through 14 October.

## Recommendations
Although there is a difference in cumulative proportional CPUE by fish wheel for years 2016 and 2017 when the data are analyzed on a yearly basis (Figure 1), the difference is minimal when the cumulative proportional CPUE is averaged over a 5, 10, or 20 year period (Figure 2, Figure 3). Therefore, it is recommended that the runtiming curve (i.e., cumulative proportional CPUE by day) averaged over 5, 10, or 20 years using data from all three fish wheels be used. 

<img src="https://github.com/SOLV-Code/Taku-Sockeye-Public/blob/master/fishwheel_cpue_archived/figs/cumsum_by_year.png"
	width="600">

Figure 1: Cumulative proportional CPUE for the Canyon Island fish wheels. The cumulative proportional CPUE for fishweels one and two are combined. For comparison, dates are standardized between fish wheel three and fish wheels one and two. The dotted line is fish wheel three and the solid line is fish wheel 1 and 2.

<img src="https://github.com/SOLV-Code/Taku-Sockeye-Public/blob/master/fishwheel_cpue_archived/figs/cumsum.png"
	width="600">
	

Figure 2: The 5 year (2014 to 2018), 10 year (2009 to 2018), and 20 year (1999 to 2018) cumulative proportional CPUE average by fish wheel. Data for fish wheels one and two (i.e. catch and effort data) are combined. 

<img src="https://github.com/SOLV-Code/Taku-Sockeye-Public/blob/master/fishwheel_cpue_archived/figs/cumsum_average.png"
	width="600">
	
Figure 3: The 5 year (2014 to 2018), 10 year (2009 to 2018), and 20 year (1999 to 2018) cumulative proportional CPUE average by fish wheel. Data for fish wheels one and two (i.e. catch and effort data) are combined. The grey line is the 20 year average, the solid black line is the 10 year average, and the dotted black line is the 5 year average,
