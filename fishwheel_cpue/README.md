# Canyon Island Fish wheel CPUE
## Background
The objective of this analysis was to create a Taku sockeye salmon run timing curve using data from the Canyon Island fish wheels. This curve may be used inseason or postseason for expansions of abundance estimates (e.g. early fish wheel removal). Data for this curve should be updated each spring to create an up to date runtiming curve.


**NOTE**: You need to extract the zipped data files before running this code. Place the extracted data files in the location: fishwheel_cpue/data.


The data sources used in this analysis are:

1. Taku_Sockeye_CYI_TagReleases.csv
   - Catch data from the fish wheels at Canyon Island downloaded from the ASL database (ADF&G database OceanAK). This includes catch from all three fish wheels by day and cannot be separated. In 2016 and 2017, there were three fish wheels. In all other years, there were only two fish wheels. This data is the same as the data in the folder DATA\BaseData\Large_Files\Taku_Sockeye_LargeRawFiles_Feb2020_\Taku_Sockeye_CYI_TagReleases, but an additional year of data is added each season.
2. Taku_Sockeye_CYI_Effort_FW1and2.csv 
   - Effort data for fish wheel one and fish wheel two.
     - This data can be found in the inseason datasheets. Effort data is the total number of hours the fish wheels were run each day.
3. Taku_Sockeye_CYI_Effort_FW3.csv 
   - Effort data for fish wheel three (years 2016 and 2017 only).
     - This data can be found in the inseason datasheets. Effort data is the total number of hours fish wheel three was run each day.
4. Taku_Sockeye_CYI_catch_FW3.csv 
   - Catch data for fish wheel three (years 2016 and 2017 only.
     - This data can be found in the inseason datasheets. This is the total catch by day from fish wheel three only. 

## Methods
First, the most recent year of fish wheel catch data needs to be updated from OceanAK and added to the dataset 'Taku_Sockeye_CYI_TagReleases.csv.' The downloaded data should contain the same variables as prior years or those columns should be left blank. Then, the input section needs to be updated to the most recent year of data (in the input section, change the current_year <- " "). Note that the catch data in the OceanAK database (i.e. ASL data) from 1985 through 2015 and 2018-2019 only contains catch from fish wheels one and two, while catch data from years 2016 and 2017 includes catch from fish wheels one, two, and three combined. Effort data (number of hours the fish wheel ran per day) is from inseason datasheets and is separated by fish wheel. The most current data (2019 on) only contains data from two fish wheels.

## Results
To create the runtiming curve, use the file CYI_cpue_FW123.csv that is created. The columns cpue_data_5yr, cpue_data_10yr, and cpue_data_20yr are the rolling averages for the specified number of years using the column cumsum_prop_cpue. 


<img src="https://github.com/SOLV-Code/Taku-Sockeye-Public/blob/master/fishwheel_cpue/figs/cumsum.png"
	width="600">


Figure 1: The 5, 10, and 20 year cumulative proportional CPUE average by fish wheel. 

