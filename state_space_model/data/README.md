# State Space Model Data
The two data files needed for the state space model are the Taku_sockeye.csv file and the ir.csv file. Any reference to border pertains to the US/Canada border on the Taku River.

The Taku_sockeye.csv file includes year, wild harvest below the border (hbelow_wild), wild harvest above the border (habove_wild), harvest above the border (habove), and harvest below the border (hbelow). In addition, it includes age compositions from ages 2-4 (x4), age 5 (x5), and ages 6-8 (x6), and coefficients of variation (cv) from harvest below (cv.hb) and above the border (cv.ha). Detailed descriptions of the variables are found in the report Miller and Pestal 2020.

The raw data for the harvest are found in the report TTC (2019).

The appendicies are also in excel format and housed at ADF&G by the Taku/Stikine commercial fisheries biologist in Douglas, Alaska. Therefore, reference to the excel column (A through Z) are also given below.

## Data Sources
In the report TTC (2019), the raw data for the following variables are:

1. hbelow_wild variable (all wild harvest below border (wild commercial gillnet harvest in D111, wild amalga seine harvest), but do not include the US personal use wild harvest)
   - Appendix D7 (D111 Amalga Seine harvest (wild Taku); column 7; column G in actual tables)  
   - Appendix D8 (D111 gillnet harves (Taku wild); column 8; column H in actual tables)

2. hbelow variable (all wild and enhanced harvest below border this includes commercial gillnet from D111, Amalga seine harvest, and US personal harvest)
   - hbelow_wild 
   - personal use (wild; Appendix D7; column 10; column J in actual tables)
   - enhanced fish from D111 Amalga seine harvest, personal use Taku harvest, and Tatsamenie, Little Trapper and King Salmon
     - Appendix D7 (column 8; column H in actual tables)
     - Appendix D7 (column 11; column K in actual tables)
     - Appendix D8 (columns 5-7; columns E-G in actual tables)   
   
3. habove_wild variable (all wild harvest above border including wild inriver commercial, wild aboriginal, wild test, and wild broodstock)
   - wild harvest
     - Appendix D10 (columns 7-9; columnns G-I in actual tables) 
   - wild broodstock 
     - Appendix D12 (column 5; column E in actual tables)
     - Appendix D13 (column 3; column C in actual tables)
     - Appendix D14 (column 3; column C in actual tables)    
     
4. habove (all commercial, aboriginal, test harvest but exclude broodstock take)
   - wild harvest
     - Appendix D10 (columns 7-9; columnns G-I in actual tables) 
   - enhanced fish from inriver commercial, test, and aboriginal
     - Appendix D10 (columns 10-12; columns J-L)

The ir.csv file contains the capture-recapture abundance estimates from 1984 through 2018 and the associated coefficients of variation (cv.ir).


The age_comp_raw_data.xlsx file is an excel file the shows the calculation steps in order to create the variables x4, x5, and x6 in the Taku_sockeye.csv file. 

## References
[Miller and Pestal, 2020](https://www.dfo-mpo.gc.ca/csas-sccs/Publications/ResDocs-DocRech/2020/2020_035-eng.html)  
[Transboundary Technical Committee (TTC) 2019](https://www.psc.org/download/43/transboundary-technical-committee/11711/tctr-19-2.pdf)
