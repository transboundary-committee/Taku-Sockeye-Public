# State Space Model

The original state space model code was adapted from Fleischman et al. 2013. This code was created to run a Bayesian state space model for Taku River sockeye salmon.

The state space model folder is made up of a code, data, and output folder. To run the model, run the code 1_RUN_MODEL.R. This will automatically run the 2a_GENERATE_OUTPUTS.R code. To generate figures, then run the 3_GENERATE_FIGURES.R code.

Within the code 1_RUN_MODEL.R, there are choices for out.label, package.use, jags.settings, and sensitivity.analysis. It is best to run the "test" settings first to determine if there are any bugs in your code. 
The options in the code are:

```
out.label <-  "rjags_Explore_BaseCase" #"R2Jags_Explore_BaseCase" or #"rjags_Explore_BaseCase" # label to be used for the output folder (and for scenario comparisons)
package.use <- "rjags"  #"rjags"  or "R2jags"
jags.settings <- "test"  # "test" or "explore" or full" 
sensitivity.analysis <- 0 #0; 1 is yes and 0 is no
```
The code chunk above correponds to these options:
1. rjags run 
   - out.label <-  "rjags_Explore_BaseCase"  
   - package.use <- "rjags"    
   - jags.settings <- "test"  # "test" or "explore" or full" 
   - sensitivity.analysis <- 0 "    
   
2. R2jags run 
   - out.label <-  "R2Jags_Explore_BaseCase"   
   - package.use <- "R2jags"    
   - jags.settings <- "test"  # "test" or "explore" or full" 
   - sensitivity.analysis <- 0 " 
   
3. rjags run with sensitivity analysis of beta prior [step #4 in the code]  
   - out.label <-  "rjags_Explore_BaseCase" 
   - package.use <- "rjags"    
   - jags.settings <- "test"  # "test" or "explore" or full" 
   - sensitivity.analysis <- 1 " 
   

In the 3_GENERATE_FIGURES.R code, the input values need to be specified first. Based on the output of the state space model, the input values are:  

```
LowerB <- 49500  #lower bound of recommended escapement goal range  
UpperB <- 55700 #upper bound of recommended escapement goal range  
SMSY <- 43857  #Lambert W from lambert file  
UMSY <- 0.75  #median from staquants file   
SMAX <- 59145  #median from staquants file  
SEQ <- 124106 #median from staquants file  
lnalpha.c <-  2.1115 #median from staquants file  
beta <-1.69076E-05  #median from staquants file  
SGEN<-5873  
```

In lines 55 and 56, the i, z, xa.start, and xa.end can be adjusted depending on the data set. The figures code also contains code for creating french figures.

```
profile(i=10, z=50, xa.start=0, xa.end=8000,lnalpha.c, beta)
```

## References
Fleischman S. J., M. J. Catalano, R. A. Clark, and D. R. Bernard 2013. An age-structured state-space stock-recruit model for Pacific salmon (*Oncorhynchus* spp.) Canadian Journal of Fisheries and Aquatic Sciences 70: 401-414.