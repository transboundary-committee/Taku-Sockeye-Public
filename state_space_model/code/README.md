---
output:
  pdf_document: default
  html_document: default
---
# Drop Out Rate Analysis 
Author: Carl schwarz and adapted by Sara Miller
Updated: May 2020

Model the dropout rate using a "fake" binomial distribution. There are four ways to model dropout probabilities allowing for different levels of variation and uncertainty
(1) naive - assume that drop out rate is equal in all years and just pool the data
(2) use the average dropout over all years but ignoring any year-to-year variation
(3) use the average dropout over all years but allow for year-to-year variation but no estimation error in the dropout 
(4) use the average dropout over all years but allow for year-to-year variation and estimation error in the average

The method (average dropout over all years but allow for year-to-year variation and estimation error in the average) 
was used to determine the dropout rate for 2019 (n=51, x=13) based on data from years 1984, 2015, 2017, and 2019 (historical method).

NOTE: The same equations at the end of the document can be used to adjust SPAS estimates or even BTSPAS estimates computed without adjusting for dropout, but in the latter case, the BTSPAS package has a specialized function to do that already.

How to model different levels of variation:
The key is that binomial std of p=x/n is sqrt(p(1-p)/n)
So if you want p=.20 with se=.02, you want to find x=.2n and n such that
       sqrt(p(1-p)/n) = .02 
       sqrt(.2(.8)/n) = .02
       .2(.8)/n    = .02^2
       n= .2(.8)/.02^2 = 400 and x=.2(400)=80

Decomposition of drop out rates into year-to-year variation and sampling variation

