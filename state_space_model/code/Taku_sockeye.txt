model
{
    for (y in (A + a.min):(Y + A - 1)) {
        log.R[y] ~ dnorm(log.R.mean2[y], tau.R)
        R[y] <- exp(log.R[y])
        log.R.mean1[y] <- log(S[y - a.max]) + lnalpha - beta * 
            S[y - a.max]
        log.resid[y] <- log(R[y]) - log.R.mean1[y]
    }
    log.R.mean2[A + a.min] <- log.R.mean1[A + a.min] + phi * 
        log.resid.0
    for (y in (A + a.min + 1):(Y + A - 1)) {
        log.R.mean2[y] <- log.R.mean1[y] + phi * log.resid[y - 
            1]
    }
    lnalpha ~ dnorm(0.00000E+00, 1.00000E-06)  T(1.00000E-06, )
    beta ~ dnorm(0.00000E+00, 1.00000E-06)  T(1.00000E-06, )
    phi ~ dnorm(0.00000E+00, 1.00000E-06)  T(-1, 1)
    mean.log.RO ~ dnorm(0.00000E+00, 1.00000E-06)
    tau.RO ~ dgamma(0.001, 0.001)
    log.resid.0 ~ dnorm(0.00000E+00, tau.red)
    tau.R ~ dgamma(0.001, 0.001)
    sigma.R <- 1/sqrt(tau.R)
    alpha <- exp(lnalpha)
    sigma.RO <- 1/sqrt(tau.RO)
    tau.red <- tau.R * (1 - phi * phi)
    lnalpha.c <- lnalpha + (sigma.R * sigma.R/2/(1 - phi * phi))
    R.O <- exp(mean.log.RO)
    for (y in 1:a.max) {
        log.R[y] ~ dnorm(mean.log.RO, tau.RO)
        R[y] <- exp(log.R[y])
    }
    S.max <- 1/beta
    alpha.c <- min(exp(lnalpha.c), 10000)
    S.eq.c <- lnalpha.c * S.max
    U.msy.c <- lnalpha.c * (0.5 - 0.07 * lnalpha.c)
    S.msy.c <- S.eq.c * (0.5 - 0.07 * lnalpha.c)
    positive.lna.c <- step(lnalpha.c)
    lnalpha.c.nonneg <- lnalpha.c * positive.lna.c
    S.eq.c2 <- lnalpha.c.nonneg * S.max
    peterman.approx.c <- (0.5 - 0.65 * pow(lnalpha.c.nonneg, 
        1.27)/(8.7 + pow(lnalpha.c.nonneg, 1.27)))
    U.msy.c2 <- lnalpha.c.nonneg * peterman.approx.c
    S.msy.c2 <- U.msy.c2/beta
    U.max.c2 <- 1 - 1/exp(lnalpha.c.nonneg)
    S.msy.c.80 <- S.msy.c * 0.8
    S.msy.c2.80 <- S.msy.c2 * 0.8
    D.scale ~ dunif(0.00000E+00, 1)
    D.sum <- 1/(D.scale * D.scale)
    pi.2p ~ dbeta(1, 1)
    pi.1 ~ dbeta(1, 1)
    pi[1] <- pi.1
    pi[2] <- pi.2p * (1 - pi[1])
    pi[3] <- 1 - pi[1] - pi[2]
    for (a in 1:A) {
        gamma[a] <- D.sum * pi[a]
        for (y in 1:(Y + A - 1)) {
            g[y, a] ~ dgamma(gamma[a], 0.01)
            p[y, a] <- g[y, a]/sum(g[y, ])
        }
    }
    for (a in 1:A) {
        for (y in a:(Y + (a - 1))) {
            N.ya[y - (a - 1), (A + 1 - a)] <- p[y, (A + 1 - a)] * 
                R[y]
        }
    }
    for (y in 1:Y) {
        N[y] <- sum(N.ya[y, 1:A])
        for (a in 1:A) {
            q[y, a] <- N.ya[y, a]/N[y]
        }
    }
    for (t in 1:Y) {
        x[t, 1:A] ~ dmulti(q[t, ], n.a[t])
    }
    for (y in 1:Y) {
        mu.hbelow_wild[y] ~ dbeta(1, 1)
        h.below_wild[y] <- mu.hbelow_wild[y] * N[y]
        log.hb_wild[y] <- log(h.below_wild[y])
        tau.log.hb.wild[y] <- 1/log(cv.hb[y] * cv.hb[y] + 1)
        hbelow_wild[y] ~ dlnorm(log.hb_wild[y], tau.log.hb.wild[y])
        mu.habove_wild[y] ~ dbeta(1, 1)
        h.above_wild[y] <- mu.habove_wild[y] * N[y]
        log.ha_wild[y] <- log(h.above_wild[y])
        tau.log.ha.wild[y] <- 1/log(cv.ha[y] * cv.ha[y] + 1)
        habove_wild[y] ~ dlnorm(log.ha_wild[y], tau.log.ha.wild[y])
        inriver.run[y] <- max(N[y] - h.below_wild[y], 1)
        log.ir[y] <- log(inriver.run[y])
        tau.log.ir[y] <- 1/log(cv.ir[y] * cv.ir[y] + 1)
        ir[y] ~ dlnorm(log.ir[y], tau.log.ir[y])
        mu.habove[y] ~ dbeta(1, 1)
        h.above[y] <- mu.habove[y] * inriver.run[y]
        log.ha[y] <- log(h.above[y])
        tau.log.ha[y] <- 1/log(cv.ha[y] * cv.ha[y] + 1)
        habove[y] ~ dlnorm(log.ha[y], tau.log.ha[y])
        mu.hbelow[y] ~ dbeta(1, 1)
        h.below[y] <- mu.hbelow[y] * N[y]
        log.hb[y] <- log(h.below[y])
        tau.log.hb[y] <- 1/log(cv.hb[y] * cv.hb[y] + 1)
        hbelow[y] ~ dlnorm(log.hb[y], tau.log.hb[y])
        mu[y] <- (h.below[y] + h.above[y])/N[y]
        S[y] <- max(inriver.run[y] - h.above[y], 1)
        log.S[y] <- log(S[y])
        lnRS[y] <- log(R[y]/S[y])
    }
}
