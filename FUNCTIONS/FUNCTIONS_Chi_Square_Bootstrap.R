##############################################################################
#  Chi-Squared Test

chi.boot <- function(X,boot.size=1000, boot.prop=0.9,continuity.correct=FALSE){
# X is a matrix with 2 columns and two levels in each column
# bootstrapping is done on the first column
	X <- X[order(X[,1]),] # SORT baseD on col 1
	
	library(moments)
	
	prop.test.out <- prop.test(table(X ),correct=continuity.correct)

	x1.levels <- unique(X[,1])
	
	x1.L1.mat <- X[ X[,1]== x1.levels[1],]
	L1.n <- dim(x1.L1.mat)[1]
	x1.L2.mat <- X[ X[,1]== x1.levels[2],]
	L2.n <- dim(x1.L2.mat)[1]

	p.values.boot <- rep(NA,boot.size)
	for(i in 1:boot.size){
		L1.sub <- x1.L1.mat[sample(L1.n,L1.n*boot.prop),]
		L2.sub <- x1.L2.mat[sample(L2.n,L2.n*boot.prop),]
		x.sub <- rbind(L1.sub, L2.sub)
		p.values.boot[i] <- prop.test(table(x.sub),correct=continuity.correct)$p.value	
	}

out.list <- list( est= prop.test.out$estimate,
				p.value= prop.test.out$p.value,
				 prop.sig.p.values = sum(p.values.boot<=0.05)/boot.size,
				conf.int.sample.diff= prop.test.out$conf.int,
				sample.summary = table(X ) #, bootstrapped.p.values = p.values.boot
				)

return(out.list)

}