
# STAT WEEK CALCULATOR
# using this : https://stackoverflow.com/questions/17286983/calculate-statistical-week-starting-1st-january-as-used-in-fisheries-data


ufmt <- function(x) as.numeric(format(as.Date(x), "%U"))


statweek.calc <- function(date.in){

# date.in is text string (or vector of strings) in date format (e.g. "2013-01-02")

date.use <- as.Date(date.in)
stat.week <- ufmt(date.use) - ufmt(cut(date.use , "year")) + 1

return(stat.week)

}



convert.julian <- function(day, year){

# day is the julian day number
# year is the year 
 
# assumes Jan 1 = Day 1 
date.out <- as.Date(day, origin = paste0(year-1,"-12-31")) 
return(date.out)  

# checked against lubridate::yday()
# convert.julian(167,1985)
#[1] "1985-06-16"
# lubridate::yday("1985-06-16")
#[1] 167

  
}







.isleapyear <- function(year){
  # http://rss.acs.unt.edu/Rdoc/library/fame/html/isLeapYear.html
  ifelse(year%%4 == 0 & (year%%100 != 0 | year%%400 == 0),TRUE,FALSE)
}





date.calc <- function(year,week,day){

# year , week, and day are numerical values (or vectors of equal length)
# for example: year = 2012, week = 26, day = 4 
# the function then calculates the date for the 4th day of the 26th stat week in 2012

if( length(unique(c(length(year) ,length(week),length(day))))>1){warning("all inputs must have same length!");stop()}

# ORIGINAL VERSION -> MESSING UP SOME YEARS
# code adapted from http://r.789695.n4.nabble.com/Convert-week-value-to-date-td3751170.html
# leap.yr.check <- .isleapyear(year)# temporary patch for leap year issue!!!! -> need to follow up
# date.out <- rep(NA,length(year))
# for(i in 1:length(year)){
# if(leap.yr.check[i]) {adj.value <- 2 }  # <- 2 not 0 !!!!!
# if(!leap.yr.check[i]) {adj.value <- 2 }
# s <- seq(as.Date(paste0(year[i],"-01-01")), as.Date(paste0(year[i],"-12-31")), by = "day") #produce all days of the year
# series.sun <-data.frame(day.sunday=s[format(s, "%w") == "0"],week.val= adj.value +  as.integer(format(s[format(s, "%w") == "0"],"%W"))) #calc week value for every day
#print(series.sun)
# date.out[i] <-   as.character(series.sun[series.sun[,"week.val"]==week[i],"day.sunday"] + day[i]-1 ) # WHY NEED THE -1 HERE? -> Because R has Sun=0, Mon=1 etc
# }


date.out <- rep(NA,length(year))

for(i in 1:length(year)){

	all.dates <- seq(as.Date(paste0(year[i],"-01-01")), as.Date(paste0(year[i],"-12-31")), by = "day") #produce all days of the year
	all.weekdays <- weekdays(all.dates)
	sun.flag <- all.weekdays == "Sunday"

	if(sun.flag[1]){ adj.value <- 0 } # if Jan 1 is a Sunday, then that day starts stat week 1
	if(!sun.flag[1]){  adj.value <- 1  } # if Jan 1 is NOT a Sunday, then the first Sunday is the start of stat week 2

	sun.vec <- all.dates[sun.flag]    
	wk.vec <- (1:length(sun.vec)) + adj.value
	
	if(!is.na(week[i])){
				date.calc.out<- as.character(as.Date(sun.vec[wk.vec == week[i]] + day[i]-1 ) )  # need -1 because Sun is already 1

				# don't need this anymore, because checking for NA above			
				if(length(date.calc.out)==1) {date.out[i] <-    date.calc.out}
				if(length(date.calc.out)!=1) {print("---------");print(year[i]);print(week[i]);print(day[i]);print(date.calc);stop()}
			}
				
	if(is.na(week[i])){date.out[i] <- NA}
	
	
	
}




return(date.out)



}





fixDate <- function(x){
# x is a vector with various text strings,
# some in proper date format like "2010-09-24" , others in 
# unrecognizable format like "9/24/2010"
# this function finds and fixes the unrecognizable ones
	fix.idx <- grepl("/",x)
	x.out <- x
	x.out[fix.idx] <- as.character(as.Date(x.out[fix.idx],format = "%m/%d/%Y" )) 
	
		# need to convert to character, else get number output instead of date
		# https://stackoverflow.com/questions/39458989/why-is-as-date-is-being-returned-as-type-double
	return(x.out)
}

# testing date.fix function
#fixDate(c("7/29/2003","9/20/2003","9/21/2003" ))


extractYear <- function(x){
# x is a vector with text strings of standard date format like "2010-09-25"
	x.out <- as.numeric(format(as.Date(x, format="%Y-%m-%d"),"%Y"))
	return(x.out)
	}


#testing the year.extract.fun
extractYear(c("2003-08-10","2003-08-11","2003-08-12"))
#fixDate(recoveries.file$Start.Date[1:100])
#fixDate(recoveries.file$End.Date[1:100])



#######################################################################################


fixRecoveryFile <- function(X, tracking.folder = NULL,tracing=FALSE){
# X is the master file of recoveries read in with read.csv()
# tracking.folder  is either NULL or a file path for storing the tracking info


#options(warn=2)  # this turns warnings into errors that stop the code


# 1) fix formatting from "9/24/2010" to 2010-09-24      
X$Start.Date <- fixDate(X$Start.Date)
X$End.Date <- fixDate(X$End.Date)

if(tracing){
print("Step 1: Fix Date Formatting -------------")
print(X$Start.Date[1:100])
print(X$End.Date[1:100]) 
}

# 2) fix any end dates that were "0:00" and now show up as "1900-01-00"
#    or that were "-", "U"
zero.date.idx <- X$End.Date %in% c("1900-01-00","-","U")
print(paste("Non-valid text strings:",sum(zero.date.idx,na.rm=TRUE )))
if(!is.null(tracking.folder)){write.csv( X[zero.date.idx,]  ,paste0(tracking.folder,"/RecoveryFile_Step2_InvalidDateStrings.csv"))}
X$End.Date[zero.date.idx] <- NA

if(tracing){
print("Step 2: Fix Zero End Dates -------------")
print(X$End.Date[1:100]) 
}


# 3) change to NA any end dates that are earlier than the start dates
invalid.enddate.idx <-  ( as.Date(X$End.Date) < as.Date(X$Start.Date)  ) & !is.na(X$End.Date)
print(paste("Invalid end dates:",sum(invalid.enddate.idx,na.rm=TRUE )))
#print(invalid.enddate.idx)
if(!is.null(tracking.folder)){write.csv( X[invalid.enddate.idx ,]  ,paste0(tracking.folder,"/RecoveryFile_Step3_InvalidEndDates.csv"))}
X$End.Date[invalid.enddate.idx] <- NA

if(tracing){
print("Step 3: Fix Invalid End Dates -------------")
print(X$End.Date[1:100]) 
}


# 4) change to NA any date values that are not in the "year" of the earlier column (0:00 values that got converted to 1900-01-01 etc)
invalid.yr.start.idx  <- extractYear(X$Start.Date) != X$year  & !is.na(X$Start.Date)
invalid.yr.end.idx  <- extractYear(X$End.Date) != X$year & !is.na(X$End.Date)
print(paste("Wrong year in start date:",sum(invalid.yr.start.idx,na.rm=TRUE )))
print(paste("Wrong year in end date:",sum(invalid.yr.end.idx,na.rm=TRUE )))
if(!is.null(tracking.folder)){write.csv( X[invalid.yr.start.idx | invalid.yr.end.idx,]  ,paste0(tracking.folder,"/RecoveryFile_Step4_YearMismatch.csv"))}
X$Start.Date[invalid.yr.start.idx] <- NA
X$End.Date[invalid.yr.end.idx] <- NA

if(tracing){
print("Step 4: Fix Invalid Years -------------")
print(X$Start.Date[1:100])
print(X$End.Date[1:100]) 
}


# 5) change any end date that's NA to be the same as the start date 
enddate.na.idx <- is.na(X$End.Date) & !is.na(X$Start.Date)
print(paste("end date NA:",sum(enddate.na.idx,na.rm=TRUE )))
if(!is.null(tracking.folder)){
			write.csv( X[enddate.na.idx ,]  ,paste0(tracking.folder,"/RecoveryFile_Step5_NAEndDate.csv"))
			write.csv( table(X[enddate.na.idx ,"Fishery.Type.Escapement"]),paste0(tracking.folder,"/RecoveryFile_Step5_NAEndDate_TypeSummary.csv") ,row.names=FALSE  )
			
			}
X$End.Date[enddate.na.idx] <- X$Start.Date[enddate.na.idx] 

if(tracing){
print("Step 5: NA ENd Date -> Start Date -------------")
print(X$Start.Date[1:100])
print(X$End.Date[1:100]) 
}




# 6) calculate Start.Date and End.Date based on Start.SW and End.SW where available
print("starting step 6")

need.start.date.idx <- is.na(X$Start.Date)
need.end.date.idx <- is.na(X$End.Date)
have.sw.start.idx <- !is.na(X$Start.SW) & !is.na(X$Start.Day)
have.sw.end.idx <- !is.na(X$End.SW) & !is.na(X$End.Day)
tmp.idx.start <- need.start.date.idx & have.sw.start.idx
tmp.idx.end <- need.end.date.idx & have.sw.end.idx
print(paste("Get Start.Date from stat week and day num:",sum(tmp.idx.start ,na.rm=TRUE )))
print(paste("Get End.Date from stat week and day num:",sum(tmp.idx.end ,na.rm=TRUE )))

#print(as.numeric(X$year[tmp.idx.end]))
#print(as.numeric(X$End.SW[tmp.idx.end]))
#print(as.numeric(X$End.Day[tmp.idx.end]))



#print("end date calc")
#print(sum(is.na(have.sw.end.idx)))
X$End.Date[tmp.idx.end] <- date.calc(as.numeric(X$year[tmp.idx.end]), as.numeric(X$End.SW[tmp.idx.end]),as.numeric(X$End.Day[tmp.idx.end]))
#print(head(date.calc(as.numeric(X$year[tmp.idx.end]), as.numeric(X$End.SW[tmp.idx.end]),as.numeric(X$End.Day[tmp.idx.end]))))
#print(head(X$End.Date[tmp.idx.end]))
# NOTE: as.numeric() gives warning on "coerced NA" for End.SW and End.Day (and same for start dates below)
#        is this because of the dealing with blanks using na.strings argument when the input is read in?
#        either way, shouldn't make a diff to the end product

#print("start date calc")
X$Start.Date[tmp.idx.start] <- date.calc(as.numeric(X$year[tmp.idx.start]), as.numeric(X$Start.SW[tmp.idx.start]),as.numeric(X$Start.Day[tmp.idx.start]))


if(tracing){
print("Step 6: Calc Start/End Dates from Stat week and Day -------------")
print(X$Start.Date[1:100])
print(X$End.Date[1:100]) 
}


# 7) calculate recovery date based on Start.Date and End.Date where available

# OLD VERSION: USING MID-POINT (switched away from this based on Cdn WG feedback Jan 21,2018)
#   	specific calculation is as follows: RecoveryDate = StartDate + Round((StartDate-EndDate)/1.99)	
#			- This gives the mid-point, but rounds up from half-days
#           -  if opening is Sun-Tue, recovery date is Mon
#           -   if opening is  Thu-Fri, recovery date is Fri
#           - the division by 1.99 is just a quick way to force it to round up (b/c R's round function rounds down from 0.5)		
#need.rec.date.idx <- is.na(X$RecoveryDate)
#have.startandenddate.idx <- !is.na(X$Start.Date) & !is.na(X$End.Date)
#tmp.idx <- need.rec.date.idx & have.startandenddate.idx
#print(paste("Get RecoveryDate from End.Date and Start.Date:",sum(tmp.idx,na.rm=TRUE )))
#X$RecoveryDate[tmp.idx] <-  as.character(as.Date(X$Start.Date[tmp.idx]) +  round((as.Date(X$End.Date[tmp.idx]) - as.Date(X$Start.Date[tmp.idx]))/1.99))


# NEW VERSION: USING END DATE (Most records are coded for 24hr period noon-noon, not multi-day openings)

need.rec.date.idx <- is.na(X$RecoveryDate)
have.enddate.idx <-  !is.na(X$End.Date)
tmp.idx <- need.rec.date.idx & have.enddate.idx
print(paste("Get RecoveryDate from End.Date:",sum(tmp.idx,na.rm=TRUE )))
X$RecoveryDate[tmp.idx] <-  X$End.Date[tmp.idx]

if(tracing){
print("Step 7: RecoveryDate from EndDate -------------")
print(X$Start.Date[1:100])
print(X$End.Date[1:100]) 
print(X$Recovery.Date[1:100]) 
}


return(X)

}




exp.tagid <- function(begin.vec,end.vec,fw.list,date.in){
	if(length(begin.vec)!=length(end.vec)){warning("input vectors have to be of same length");stop()}
	out.df <- data.frame(date = NA, tag_id = NA,fishwheel=NA)

	for(i in 1:length(begin.vec)){
		out.df <- rbind(out.df,data.frame(date= date.in,tag_id = begin.vec[i]:end.vec[i],fishwheel = fw.list[i] ))
		}

	dupl.idx <- duplicated(out.df$tag_id)
	if(sum(dupl.idx) > 0){warning("Duplicate Tag ID encountered.")}
	dupl.vec <- out.df$tag_id[dupl.idx]

	out.df <- out.df[-1,]
	return(list(tag.id = out.df,dupl.id = dupl.vec))
} # end expand.tagid fn


#test the fn
#exp.tagid(c(1200,3400),c(1234,3487),fw.list=c("fw1","fw2"),date.in = "2020-12-12")
#exp.tagid(c(1200,1230),c(1234,1245),fw.list=c("fw1","fw2"),date.in = "2020-12-12")





###################################################################

fixCatchFile <- function(catches.in, day.adjust = 0){

# use day.adjust to match catch dates with recovery dates
# e.g. catches are recorded as "Day 1" for a fishery from Sun noon to Mon noon
# the corresponding date in the releases and recoveries is "Day 2"
# so the default adjustment to match them up is "+1"

# BUT: 
# What to do with catches recorded as sw27 day 7?
# Bump them to  day 1 of the next SW?
# In that case,  catches from sat noon (week26) to sun noon (week 27)
# would be treated as SW 27.
# -> Until this is resolved, use 0 adjustment
# note: date.calc() works with input day = 8, and bumps the date into the next week 
#       but the stat week doesn't get changed in the statweek colum!


# drop rows and columns that are all 0

col.idx <- grep("Y",names(catches.in))
rows.drop <- rowSums(catches.in[col.idx]) ==0
cols.drop <- colSums(catches.in[col.idx]) ==0
catches.use <- catches.in[!rows.drop,!cols.drop]

# extract the available years
ct.yrs <- names(catches.use)
ct.yrs <-   as.numeric( substr(ct.yrs[col.idx],2,5))

days.vec <- paste(catches.in$StatWeek, catches.in$StatWeekDay+day.adjust,sep="-")


yr_wk_d.mat<- expand.grid(ct.yrs,days.vec)

#print(yr_wk_d.mat)

out.mat <- matrix(NA,ncol=5,nrow=dim(yr_wk_d.mat)[1],
		dimnames=list(dimnames(yr_wk_d.mat)[[1]],c("Year","Week","Day","Date","CdnCommCatch"))
		)

out.mat <- as.data.frame(out.mat)

out.mat[,"Year"] <- yr_wk_d.mat[[1]]
out.mat[,"Week"] <- as.numeric(substr(as.vector(yr_wk_d.mat[[2]]),1,2))
out.mat[,"Day"] <- as.numeric(substr(as.vector(yr_wk_d.mat[[2]]),4,4))

#print(head(out.mat))
out.mat[,"Date"] <- date.calc(year=out.mat[,"Year"],week=out.mat[,"Week"],day=out.mat[,"Day"])


for(yr.use in unique(out.mat[,"Year"])){
for(wk.use in unique(out.mat[,"Week"])){
for(day.use in 1:7){

#print(paste("---",yr.use, wk.use, day.use,sep="-"))

out.mat[out.mat[,"Year"] == yr.use & out.mat[,"Week"] == wk.use & out.mat[,"Day"] == day.use,
                "CdnCommCatch"] <- catches.in[catches.in[,"StatWeek"]==wk.use & 
                                                catches.in[,"StatWeekDay"]==day.use  , paste0("Y",yr.use)]

}}}


# sort and filter out 0 and NA catch records
out.mat <- out.mat[order(out.mat[,"Year"],out.mat[,"Week"],out.mat[,"Day"]),]
out.mat <- out.mat[out.mat[,"CdnCommCatch"]>0,]
out.mat <- out.mat[!is.na(out.mat[,"CdnCommCatch"]),]

return(out.mat)
} # end fixCatchFile()



#########################################################

createAnnualCatchFiles <- function(catch.data,prop.large.data,out.folder){
# catch.data is an object created by fixCatchFile()


yrs.catch <- sort(unique(catch.data$Year))

for(yr in yrs.catch){

# create template
start.date <- date.calc(yr,20,1)
end.date <- date.calc(yr,40,7)

dates.vec <- as.character(seq(as.Date(start.date),as.Date(end.date),by=1))

tmp.df <- data.frame(Date = dates.vec, Year = rep(yr,length(dates.vec)), StatWeek =  rep(NA,length(dates.vec)),
			 WeekDay =  rep(NA,length(dates.vec)), HalfWeek = NA, Opening = NA, 
								CdnCommCt_All =  rep(0,length(dates.vec)),PropLarge = rep(0,length(dates.vec)),
								CdnCommCt_Large =  rep(0,length(dates.vec)),
								CdnCommCt_Small =  rep(0,length(dates.vec))
			)

for(date in dates.vec){

tmp.idx <- tmp.df$Date == date
tmp.df[tmp.idx, "StatWeek"] <- statweek.calc(date)
tmp.df[tmp.idx, "WeekDay"] <- weekdays(as.Date(date))

ct.idx <- catch.data$Date == date
if(sum(ct.idx)){tmp.df[tmp.idx, "CdnCommCt_All"] <-  catch.data$TotalInspected[ct.idx]}  # Changed May 2019 from: catch.data$CdnCommCatch[ct.idx]}

lg.idx <- prop.large.data[,"Year"] == format(as.Date(date),"%Y") &  prop.large.data[,"StatWeek"] == statweek.calc(date)
if(sum(lg.idx)){tmp.df[tmp.idx, "PropLarge"] <- prop.large.data[lg.idx, "PropLarge"]}


} # end looping through dates


# add halfweek classifications

ct.idx <- tmp.df$CdnCommCt > 0 
tmp.df[ct.idx , "Opening"] <-  paste0(tmp.df$StatWeek[ct.idx],"_1")
tmp.df[!ct.idx , "Opening"] <-  paste0(tmp.df$StatWeek[!ct.idx],"_2")

SunWed.idx <- tmp.df$WeekDay %in% c("Sunday","Monday","Tuesday","Wednesday")
tmp.df[SunWed.idx, "HalfWeek"]  <-  paste0(tmp.df[SunWed.idx,"StatWeek"],"_1")
tmp.df[!SunWed.idx, "HalfWeek"]  <-  paste0(tmp.df[!SunWed.idx,"StatWeek"],"_2")


# Add small and large catch split

tmp.df[is.na(tmp.df[ , "PropLarge"] ), "PropLarge"] <- mean(tmp.df[ , "PropLarge"] , na.rm=TRUE)

tmp.df[ , "CdnCommCt_Large"] <- tmp.df[ , "CdnCommCt_All"] * tmp.df[ , "PropLarge"] 
tmp.df[ , "CdnCommCt_Small"] <- tmp.df[ , "CdnCommCt_All"] * (1 - tmp.df[ , "PropLarge"] )

write.csv(tmp.df,file=paste0(out.folder,"/CdnCommCatchByDay_",yr,".csv"),row.names=FALSE)

} # end looping through years

} #end function createAnnualCatchFiles 






