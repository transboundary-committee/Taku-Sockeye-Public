# BaseData/Prepared_Tables

This folder contains source files for markdown tables in the report.


## Taku_Sockeye_DropoutSummaryTable.csv

Summary of dropout rate estimates from telemetry studies.
For full citations, refer to  [Pestal, Schwarz, and Clark (2020)](https://www.psc.org/download/33/psc-technical-reports/12486/psc-technical-report-no-43.pdf)




