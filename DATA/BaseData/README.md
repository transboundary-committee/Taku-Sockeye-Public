# BaseData

This folder contains verified complete versions of of the basic data sets raw data inputs. All other data sets are derived from these files using R functions. For example, Cdn Commercial catch and effort by day is stored here, but a file with CPUE by stat week is generated and stored in the DATA/DerivedData folder.


## Large Files

For most files, the full version is included here, but for files larger than ~2MB, only a zipped version is tracked on github. You need to extract the zipped into your local copy of the repository in this folder before  you can run the data processing code. 

For details, refer to the *Large_Files* folder



## Other Folders

* *Lookup_Files* contains general reference files which typically don't require updating.
* *Prepared_Tables* contains source files for markdown tables in the report.








