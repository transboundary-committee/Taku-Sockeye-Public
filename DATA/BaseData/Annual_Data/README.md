# BaseData/Annual_Data

This folder contains data files that need to be updated annually.



## Taku_Sockeye_Cdn_TestFishery_Ck_SkCatch.csv & Taku_Sockeye_Cdn_TestFishery_Co_SkCatch.csv

NOTE: Two separate files, one for early-season CK test fishery, one for late season coho test fishery

IMPORTANT NOTE: 2005-2018 CK Test fishery catches are already included in the commercial catch data file.

- Used several sources of information to construct this file:
	- main source for daily data covering 1999-2004 was "Chinook Test Fishery Sockeye Catches 1999-2004_NEW.xlsx" provided by DFO Whitehorse in April/May 2019
	- checked weekly totals against "test fish catches from C&E reports.xlsx" provided by ADFG in April/May 2019, covering 1987 to 2004
	- Also did some spot-checks against annual MR worksheets and PSC reports
  
NOTE: "Inspected" = Test + Released 


## Taku_Sockeye_DropoutAdjustments_ByYear.csv

Dropout rates to be used for adjusting run size estimates. 

For each year the file lists the total number of tags (*n*), the number that dropped out (*dropout*), and the resulting dropout rate (*dr*). The long-term average dropout adjustment was modelled using synthetic values of n and x that incorporated the weighted average of the results from 1984, 2015, 2017, and the 2018 side project
radiotelemetry studies. For details, refer
to Section 4.2.4 of [Pestal, Schwarz, and Clark (2020)](https://www.psc.org/download/33/psc-technical-reports/12486/psc-technical-report-no-43.pdf)


## Taku_Sockeye_FSC.csv

- Using file provided by DFO Whithorse in March 2019
- After copying the source data into the csv, need to reorganize into a 3 column format Year, Date, Catch

NOTES:
- Prior to approximately 2010 catch is reported on an annual basis, 
and it is documented in the TTC C&E reports. 
- This includes the odd fish taken at Kuthai or King Salmon lakes 



## TakuSockeye_PublishedAnnualMREstimates.csv


- Using Table from Appendix D.15 of Annual Report

In Excel:
- fix dates to short format 
- change number formatting (123,456 -> 123456, 43% -> 0.43)
- remove table caption text on top and avg row on bottom
- fix column headers
- ExpFactor: fill in 0 for empty cells 

NOTES:
* Run estimate does not include spawning escapements below the U.S./Canada border.										
* The early season sockeye salmon expansion is based on the proportion of fish wheel sockeye salmon catch that occurs before the fishery opens.	





## Taku_Sockeye_SecondaryTagRecoveries_ByWeek.csv

This file is a manual extract of data columns from "SecondaryMarks.xlsx" 
maintained by DFO Whitehorse.

This data file only uses the following columns from the source file
* Stat week
* Canadian Catch	
* Tags Recovered
* Fish Examined for 2nd Marks	
* Number of 2nd Marks

WARNING: The Cdn Catch and Tags Recovered values here are kept in here as a comparison to the values calculated from the merged master file of recoveries and the  cdn catch by day in "Taku_Sockeye_CdnCommCatchSockeye.csv". DO NOT USE "CATCH" OR "TAGS RECOVERED" FROM THIS FILE.

The remaining columns are calculated in R and written to an output file in the "processed" folder.

NOTES:

* Blank entries for tags recovered or 2ndary marks spotted were included as 0
* The combined week 23-24 in 2005 was dropped (38 fish, 1 tag, no 2ndary marks)
