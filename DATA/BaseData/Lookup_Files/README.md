# BaseData/Lookup_Files

This folder contains general reference files which typically don't require updating.


## Lookup_AgeClasses.csv

Age class labels and corresponding numeric values for freshwater, marine, and total age.

## Lookup_LengthConversion.csv

Parameters for linear regression fits to convert alternative length measures (CAF vs. MEF).

## Lookup_Translation_Terms.csv

Contains English-French translation of key terms,
used for generating French figures. 


