# BaseData/Large_Files

**Note**: Large files are stored in a zip folders (one zip for each major data update). Extract the one you want to use, then run the code.

Shortened sample files are included in the *BaseData/LargeFiles/Samples_Files* sub-folder.


## Sources

U.S. fish wheel data (tags, ASL) are maintained in the ADF&G Zander Database (Zander 2019). Stock composition from the fish wheel data is stored in spreadsheets and summarized in the TTC reports (TTC 2019a). Since 2012 the stock identification data has been stored in ADF&G’s Gene Conservation Laboratory database. Canadian weir counts, harvest records, ASL samples, and tag data are maintained and housed in Excel format by DFO. All fishery otolith data are maintained by the ADF&G Mark Lab.


## SPAS_InputMatrices_Used_1984to2018.xlsx

This is a historical record initially compiled from recent reports and scanned notes from earlier post-season binders. Updates are identified with comments.



## Taku_Sockeye_CdnComm_ASLSamples.csv

Age/Sex/Length samples from the Canadian commercial catch, generated
from *Sockeye Cdn Fishery Sample Data.xlsx* maintained by DFO Whitehorse.

NOTES: 
* After converting the source data into the csv, need to convert "Sample Date" column to "Short Date" format.



## Taku_Sockeye_CdnTagRecoveries.csv

Tag recoveries from Canadian fisheries and spawner surveys, generated from
*Sockeye Tag Recovery Master File-SHARE.xlsx* maintained by DFO Whitehorse.

NOTES:
* add a column with cleaned tag numbers (i.e. any text comments, "?", etc are changed to NA)
* reformat the date columns to "Short Date" format in excel! (look the same after, but needed to do this for R to read it properly)




## Taku_Sockeye_Esc_ASL_Data.csv

This is a compilation of length, sex, age and tag-related data collected from Taku River spawning areas.  Approximately 90% of the data was collected at counting weirs. The remainder is directly from spawning grounds, primarily live fish captured via beach seines.  Weir data intended to be a random sample collected in proportion to run; a small component was collected from broodstock (non-random).  Spawning ground samples generally collected for the purpose of stock identification baselines (genetic, scale pattern, brain parasite).

### Source

The source file for the current version is *Taku River Sockeye Escapement ASL Data_Master_2019_12_09.csv*


### Variable Definitions


**Field Code** | **Definition**
-- | --
Record, Year, Record by Year, Location, Sub-Location, Detailed Location Descriptor | Identifiers
Waypoint  |	Coordinates recorded using a GPS device
Sample Source |	Source of sample, currently entries limited to Escapement or Broodstock
Gear	| Capture method
Sample Type	| Purpose of sample, currently entries limited to Electrophoretics or Carcass
Sample Date, Sample SW, Catch Date , Catch SW | SW = Statistical Week, catch dates are typically the same as sample dates.
Scale Book Serial Number, Scale Number, Concatenate Book/scale	| scale book sample identifiers
Length	| Undefined length data in mm
LengthMEF |	Mid-eye to fork-of-tail length in mm
LengthPOH |	Post-orbital to hypural length in mm
LengthFork |	Nose to fork-of-tail length in mm
LengthCAF |	Cleithral arch to fork-of-tail length in mm
LengthCAH |	Mid-eye to fork-of-tail length in mm
Sex	| Self-explanatory
Tag Scar | Axillary Appendage Clip, Scar from tag application, or clipped axillary appendage (a secondary mark used in conjuction with tag)
Spaghetti Tag Prefix | 	Letter preceding tag number
Spaghetti Tag Number | Individual identifier for tag
GR Age	| Age using Gilbert-Rich convention
EurAge	| Age using European convention
Age Code |	Qualifier for age data (e.g. scale resorbed) or partial age only
Condition	| Pre-/mid-/post- spawn; colouration: 1=silver bright…….
A/M/D	| Activity level - Active/Moribund/Dead
Weight |	Weight in lbs
Head#	| Labelling data for head if collected
DNA Collected |	Y/N, or labelling data
Samplers	| Individual (initials), agency, or contracting firm collecting sample
General Comments |	Ancillary remarks regarding sample
Entered By, Entry Date, Data Transferred by	, Transfer Date	, QC Comment, Record verified by , Verification Date, Age Data Entered By, Age Data Entry Date | metadata for record managagement.



## Taku_Sockeye_CYI_TagReleases.csv

This is the ADFG post-season tag release file. The data is collected at the fish wheels and entered on scantron bubble sheets that are scanned into the *OceanAK* database through the Douglas Age lab during the season. 



### Variable Definitions


**Field Code** | **Definition**
-- | --
year | year of sample
project_cc | ADF&G code--individual code is assigned to each project
Project	| Project name
Sample Number 	|  *unique ID number within a batch??**
Batch Name	| *??*
Stat Area		| The ADF&G statistical area of fishing 
Stream		| stream where the project occurs
Gear		| gear used to catch fish
Species		| fish species sampled
sample_date		| date of sample
Stat Week		| The first statistical week of the year begins on January 1 and ends on the first Saturday of the year. All subsequent statistical weeks begin on a Sunday and end on a Saturday. 
Sample Number | date of the sampled fish
Scale Card No | scale card is the unigue number associated with scales collected from the specific species. The card is a grid and 44 scales can be placed on a card
Specimen Number		| the unique number assigned to the specimen on the scale card
Sex Code		|  1=male, 2=femal
Length Type 	| 	the length used to measure the fish
length_millimeters 	| 	the length of the indiviual fish measure to the nearest 5mm
Age European 	| 	age assigned to the fish. First number is freshwater age and the second number is marine age
Age Error Code		|  *??*
Spaghetti Tag No		| individual tag inserted into the fish; since 2019 the 1st digit on the tag is assigned to fish wheel 1 or 2
DNA Specimen No		| unique number assigned to the genetic tissue assigned
Specimen Comments		| have other codes like radio tag or pertinent information related to the fish

### Data Preparation 

The following modifications of the raw database extract were required:
 
- added a columns with cleaned tag numbers (i.e. any text comments,"0", "      ", "?" etc are changed to NA)
- changed the Year column to year
- changed the "Sample Date" column to "sample_date"
- changed the  "Length.mm" column to  "length_millimeters"
- removed all non-sockeye records
- fixed column headings (**DETAILS**)
- removed duplicate "Sex Code" column

WARNING: "SampleDate" formats seem to switch with file updates. Need to verify when running the processing script!

WARNING: Depending on the SQL details, the csv file may be preceded by a "UTF-8_BOM".
		(see [here]( https://stackoverflow.com/questions/24568056/rs-read-csv-prepending-1st-column-name-with-junk-text)for details). This makes the first column labels show up as ""ï..year".


### Query Details

SQL code used to generate this file from OceanAK database.
```
SELECT
   0 s_0,
   "Region I - Salmon - Biological - ASL"."Gear"."Gear" s_1,
   "Region I - Salmon - Biological - ASL"."Length Type"."Length mm" s_2,
   "Region I - Salmon - Biological - ASL"."Length Type"."Length Type" s_3,
   "Region I - Salmon - Biological - ASL"."Location"."Stat Area" s_4,
   "Region I - Salmon - Biological - ASL"."Location"."Stream" s_5,
   "Region I - Salmon - Biological - ASL"."Miscellaneous"."Dna Specimen No" s_6,
   "Region I - Salmon - Biological - ASL"."Miscellaneous"."Spaghetti Tag No" s_7,
   "Region I - Salmon - Biological - ASL"."Project"."Project Code" s_8,
   "Region I - Salmon - Biological - ASL"."Project"."Project" s_9,
   "Region I - Salmon - Biological - ASL"."Sample"."Batch Name" s_10,
   "Region I - Salmon - Biological - ASL"."Sample"."Sample Date" s_11,
   "Region I - Salmon - Biological - ASL"."Sample"."Sample Number" s_12,
   "Region I - Salmon - Biological - ASL"."Sample"."Stat Week" s_13,
   "Region I - Salmon - Biological - ASL"."Scales"."Age Error Code" s_14,
   "Region I - Salmon - Biological - ASL"."Scales"."Age European" s_15,
   "Region I - Salmon - Biological - ASL"."Scales"."Scale Card No" s_16,
   "Region I - Salmon - Biological - ASL"."Scales"."Specimen Comments" s_17,
   "Region I - Salmon - Biological - ASL"."Scales"."Specimen Number" s_18,
   "Region I - Salmon - Biological - ASL"."Sex"."Sex Code" s_19,
   "Region I - Salmon - Biological - ASL"."Species"."Species" s_20,
   "Region I - Salmon - Biological - ASL"."Year"."Year" s_21
FROM "Region I - Salmon - Biological - ASL"
WHERE
(("Project"."Project Code" = '322') AND ("Gear"."Gear" = 'Fish Wheel') AND ("Species"."Species" = 'Sockeye'))
ORDER BY 22 ASC NULLS LAST, 10 ASC NULLS LAST, 9 ASC NULLS LAST, 13 ASC NULLS LAST, 11 ASC NULLS LAST, 5 ASC NULLS LAST, 6 ASC NULLS LAST, 2 ASC NULLS LAST, 21 ASC NULLS LAST, 17 ASC NULLS LAST, 19 ASC NULLS LAST, 20 ASC NULLS LAST, 4 ASC NULLS LAST, 3 ASC NULLS LAST, 16 ASC NULLS LAST, 15 ASC NULLS LAST, 7 ASC NULLS LAST, 8 ASC NULLS LAST, 18 ASC NULLS LAST, 12 ASC NULLS LAST, 14 ASC NULLS LAST
FETCH FIRST 65001 ROWS ONLY
```






