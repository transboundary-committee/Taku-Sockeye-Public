# Derived Data

## CLEANED_CatchASL.csv; CLEANED_Catches.csv; CLEANED_TagData.csv

Supplementary material for the report
[Pestal et al., 2020](https://www.psc.org/download/33/psc-technical-reports/12486/psc-technical-report-no-43.pdf).


These data files are the cleaned and merged output from the data processing code, as outlined in Sec. 2.2 of the report ("Quality Control"). 

The file CLEANED_CatchASL.csv contains the detailed age, sex, and length (ASL) data from the lower Taku River Canadian commercial harvest (1980-2018) including, statistical week (SW), date, spaghetti tag number (if applicable; recovery in the fishery), sex, age (Gilbert-rich age), and length (mideye-to-forklength (MEF); mm; converted from cleithral arch to fork length (CAF)). This data is a summary of the file DATA/BaseData/Large_Files/Taku_Sockeye_CdnComm_ASLSamples.csv.

The file CLEANED_Catches.csv contains the Canadian harvest data by sector (1999-2018) including Canadian commercial harvest (CdnCommCatch), Canadian Aboriginal food, social, and ceremonial harvest (FSCCatch), early-season Chinook test fishery harvest (CKTest), and late season coho test fishery harvest (COtest). This file is a merged file from the raw data files:  
(1) DATA/BaseData/Annual_Data/Taku_Sockeye_CdnCommCatchSockeye.csv,  
(2) DATA/BaseData/Annual_Data/Taku_Sockeye_FSC.csv,  
(3) DATA/BaseData/Annual_Data/Taku_Sockeye_Cdn_TestFishery_Ck_SkCatch.csv, and  
(4) DATA/BaseData/Annual_Data/Taku_Sockeye_Cdn_TestFishery_Co_SkCatch.csv.

The file CLEANED_TagData.csv contains detailed tag data including release date and location, recovery date and location, and sockeye attributes (age, size (MEF), sex). It is a merged file from a variety of data sources:  
(1) DATA/BaseData/Large_Files/Taku_Sockeye_CdnTagRecoveries.csv,  
(2) DATA/BaseData/Large_Files/Taku_Sockeye_CYI_TagReleases.csv,  
(3) DATA/BaseData/Large_Files/Taku_Sockeye_CYI_TagReleasesV2_Spaghettip1.csv, and  
(4) DATA/BaseData/Large_Files/Taku_Sockeye_CYI_TagReleasesV2_Spaghettip2.csv.

## CdnCatchByStatWeek.csv

A summary of CLEANED_Catches.csv by year, statistical week, and total inspected fish for years 1999-2018. 

## CdnCommCatch_AgeByYear.csv

Detailed Canadian commercial catch by age (Gilbert-rich age) and year (1980-2018). This file is a summary of the file CLEANED_CatchASL.csv or the file DATA/BaseData/Large_Files/Taku_Sockeye_CdnComm_ASLSamples.csv).

## CdnCommCatch_PropLargeBySW.csv

A summary of the file DATA/BaseData/Large_Files/Taku_Sockeye_CdnComm_ASLSamples.csv, DATA/DerivedData/TagData_Releases_MedianSize.csv, and 
DATA/BaseData/Large_Files/Taku_Sockeye_CYI_TagReleases.csv.

This summary file includes year, statistical week, number of observations, and proportions of fish of a certain size for years 2003-2018. Large is defined as "above median," with year-specific median size from releases.

## CdnCommCatch_SexByYear.csv

A summary of the file CLEANED_CatchASL.csv or the file DATA/BaseData/Large_Files/Taku_Sockeye_CdnComm_ASLSamples.csv by year (1980-2018), and fish attribute (male or female). 

## CdnCommCatch_Size&AgeSamplesByYear.csv

A summary of the file DATA/BaseData/Large_Files/Taku_Sockeye_CdnComm_ASLSamples.csv. This file includes the number of paired MEF and CAF fish length samples, and the number of samples with both size and age data by year (1980-2018) from the Canadian commercial harvest.

## CdnCommCatch_SizeByYear.csv

A summary of the file DATA/BaseData/Large_Files/Taku_Sockeye_CdnComm_ASLSamples.csv. This file contains quantiles of CAF length data from the Canadian commercial harvest. 

## CdnCommCatch_SizeDistrByAgeByYear.csv
A suummary of the fish size distribution in the Canadian commercial harvest by age and year.

## DataOverview_Timeline.csv
A summary of the available data sources by year

## SizeComp_Percentiles.csv
A summary comparison of the fish length data in the Canadian commercial harvest and the releases from the fish wheels. 

## StatWeekLookup.csv
A summary table of the date of the first day of statistical week 20 (Sunday) in years 1980-2018.

## SummaryOfEstimates.csv
A summary of the postseason mark recapture estimates including pooled Petersen (PP), statistical week stratified Bayesian estimates (SW), statistical week and size stratified Bayesian estimates (SWSizeAdj), and size stratified Petersen estimates (PPSizeAdj); see Table 8 and 9 and Section 4.3 in Pestal et al. 2020. report

## TagData_InputFiles_PostSeasonFileSummaryByYear.csv
A summary file of the data contained in the postseason (tag releases at the fish wheels) input file DATA/BaseData/Large_Files/Taku_Sockeye_CYI_TagReleases.csv.  

## TagData_InputFiles_Summary.csv
A summary file of the data contained in the input files:  
(1) DATA/BaseData/Large_Files/Taku_Sockeye_CYI_TagReleases.csv,  
(2) DATA/BaseData/Large_Files/Taku_Sockeye_CYI_TagReleasesV2_Spaghettip1.csv,  
(3) DATA/BaseData/Large_Files/Taku_Sockeye_CYI_TagReleasesV2_Spaghettip2.csv, and 
(4) DATA/BaseData/Large_Files/Taku_Sockeye_CdnTagRecoveries.csv,  
including number of years covered, and valid or duplicate tag data.

## TagData_Releases_MedianSize.csv
A summary of the median fish length of tagged fish released at the fish wheels.

## TagData_Summary_ByRecStatWeek.csv
A summary of the recovery of tagged fish by statistical week (1998-2018).

## TagData_Summary_ByRelStatWeek.csv
A summary of the tagged fish by release statistical week (1998-2018).

## TagData_Summary_ByYear.csv
A summary of the release and recovery of tagged fish by year (1998-2018).

## TagData_Summary_PropRecByReleaseDate.csv
A summary of the proportion of tagged fish recovered by release date (1998-2018).

## TagData_Summary_PropRecByWeekDay.csv
A summary of the proportion of tagged fish recovered by the day of the week (1998-2018).

## TagData_Summary_TravelTime.csv
A summary of the travel time (in days) of tagged fish from release to recovery.

