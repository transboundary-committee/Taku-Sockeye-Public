# DATA

## Disclaimers

*ADF&G retains intellectual property rights to data collected by or for ADF&G. Any dissemination of the data must credit ADF&G as the source, with a disclaimer that exonerates the department for errors or deficiencies in reproduction, subsequent analysis, or interpretation. The State of Alaska, Department of Fish and Game, makes no express or implied warranties (including warranties of merchantability and fitness) with respect to the accuracy, character, function, or capabilities of the data, services, or products or their appropriateness for any particular purpose. The Department of Fish and Game is not liable for any direct, incidental, indirect, special, compensatory, consequential or other damages suffered by the user or any other person or entity from the use of or failure of the data, services, or products even if the Department of Fish and Game has been advised of the possibility of such potential loss or damage. The entire risk as to the results of the use of the data, services, or products is assumed by the user. Data may be subject to periodic change without prior notification.*

*Fisheries and Oceans Canada DFO retains intellectual property rights to data collected by or for DFO. The Data is provided “as is”, and DFO excludes all representations, warranties, obligations, and liabilities, whether express or implied, to the maximum extent permitted by law. DFO is not liable for any errors or omissions in the Data, and will not under any circumstances be liable for any direct, indirect, special, incidental, consequential, or other loss, injury or damage caused by its use or otherwise arising in connection with this Data. Data is subject to periodic change without prior notification.*


## Folder Structure


* *BaseData* folder: contains raw data files (e.g. database extracts)
* *DerivedData* folder: contains data files generated from the raw data
* *DerivedDataForReport* folder: contains generated source files for the markdown tables
* *Scripts* folder: contains the R code that generates the )derived data and tracking files. **NOTE: file paths in the scripts are set up to run from the root directory (i.e. when the *.Rproj file is opened in RStudio).** 
* *TrackingFiles* folder: contains files diagnostic files from the data processing step that generates the derived data.
* *Templates* folder: sample input templates for the [in-season BTSPAS](https://gitlab.com/transboundary-committee/Taku-Sockeye-Public/-/tree/master/inseason_BTSPAS) implementation.




