#####################################
# SETTING UP

### NOTE: THIS SCRIPT CONTINUES FROM 1_GenerateTagDataSummaries.R and 
# 1b_CreateOutputs.R1. 
# Note: the objects created by scripts 1 and 1b must still be 
# in the workspace memory!

source("FUNCTIONS/FUNCTIONS_DateHandling&FileFixing.R")
source("FUNCTIONS/FUNCTIONS_EDA_Calcs.R")
source("FUNCTIONS/FUNCTIONS_EDA_Plots.R")

# check that the objects exists
dim(tags.merged.details)
names(post.tagid.summary)
names(tagdata.summary.byyear)

names(tags.merged.details)
sum(!is.na((tags.merged.details$ReleaseGearDetail)))
sum(is.na((tags.merged.details$ReleaseGearDetail)))

library(tidyverse)


##########################################
# Annual Pattern of Releases,Recoveries, and catches


# Prep

comm.rec.idx <- !is.na(tags.merged.details$RecoveryDate) & tags.merged.details$RecoveryType=="Commercial"
comm.rec.idx[is.na(comm.rec.idx)] <- FALSE



# get date range
range(format(as.Date(tags.merged.details$ReleaseDate[comm.rec.idx]), "%m"),
					format(as.Date(tags.merged.details$RecoveryDate[comm.rec.idx]), "%m"),
				na.rm=TRUE)

#-> plot June 1 to Sep 30 each year

# add stat weeks function (adds lines to plot)
add.statweeks <- function(dates.in){
# dates.in is a vector of dates
	statweek.starts <- dates.plot[weekdays(dates.in)=="Sunday"]
	statweek.nums <- statweek.calc(statweek.starts)
	abline(v=statweek.starts,col="red",lty=2)
	text(statweek.starts,par("usr")[4],labels=statweek.nums,xpd=NA,col="red",cex=0.8,adj=c(0.5,0))
	}


# ---------------------------------------------------------------------------------------------------
## REPORT FIG 11, 13 and handout: Pattern of releases and recoveries 

for(plot.which in c("png","pdf")){
  
  
if(plot.which == "png"){ yrs.vec <- 2016:2018}  
if(plot.which == "pdf"){ yrs.vec <- sort(unique(tags.merged.details$Year))}  
    
  
if(plot.which == "pdf"){ pdf(file="DATA/ReportSupplement/TagData_Summary_SeasonProfile.pdf",
                             onefile=TRUE,height=11,width=8.5)}    
  
  
  

for(plot.yr in yrs.vec){
  
  print(plot.yr)
  
  
if(plot.which == "png"){  png(filename = paste0("DATA/ReportFigs/TagData_Summary_SeasonProfile_ForRMD_",plot.yr,".png"),
      width = 480*4, height = 480*3.7, units = "px", pointsize = 14*2.8, bg = "white",  res = NA)
      }
  
  
  par(mfrow=c(3,1))
  
  dates.plot <- seq(as.Date(paste0(plot.yr,"-06-01")),as.Date(paste0(plot.yr,"-09-30")),by="day")
  yr.idx <- tags.merged.details$Year == plot.yr
  
  
  # RELEASES
  rel.vec <- table(tags.merged.details$ReleaseDate[yr.idx])
  rel.dates <- as.Date(names(rel.vec))
  plot(dates.plot,rep(1,length(dates.plot)), type="n",xlab="Date",ylab="Tagged Fish",bty="n",ylim=c(0,max(10,rel.vec,na.rm=TRUE)))
  add.statweeks(dates.plot)
  rect(rel.dates-0.5,0,rel.dates+0.5,rel.vec,col="darkgrey",border="darkgrey")
  title(main=paste("Releases",plot.yr))
  
  
  # COMMERCIAL RECOVERIES
  
  rec.vec <- table(tags.merged.details$RecoveryDate[yr.idx & comm.rec.idx  ])
  if(length(rec.vec)>0){rec.dates <- as.Date(names(rec.vec))}
  plot(dates.plot,rep(1,length(dates.plot)), type="n",xlab="Date",ylab="Tagged Fish",bty="n",ylim=c(0,max(10,rec.vec,na.rm=TRUE)))
  add.statweeks(dates.plot)
  if(length(rec.vec)>0){rect(rec.dates-0.5,0,rec.dates+0.5,rec.vec,col="darkgrey",border="darkgrey")}
  title(main=paste("Commercial Recoveries",plot.yr))
  
  
  # COMMERCIAL Catches
  ct.yr.idx <- catches.fixed[,"Year"] == plot.yr
  ct.vec <- catches.fixed[ct.yr.idx,"CdnCommCatch"]
  if(length(ct.vec)>0){ct.dates <- as.Date(catches.fixed[ct.yr.idx,"Date"])}
  plot(dates.plot,rep(1,length(dates.plot)), type="n",xlab="Date",ylab="Fish",bty="n",ylim=c(0,max(10,ct.vec,na.rm=TRUE)))
  add.statweeks(dates.plot)
  if(length(ct.vec)>0){rect(ct.dates-0.5,0,ct.dates+0.5,ct.vec,col="darkgrey",border="darkgrey")}
  title(main=paste("Commercial Harvest",plot.yr))
  
  if(plot.which == "png"){dev.off()} # close png
  
  
} # end looping through years

  if(plot.which == "pdf"){  dev.off()} # close pdf
  
} # end plot.which


# ---------------------------------------------------------------------------------------------------
## REPORT FIG 12, 14 and handout: Pattern of releases and recoveries  by date of release


prop.rec.byday <- data.frame(TagsOut = NA, NumRecComm = NA, PercRecComm = NA)

for(plot.which in c("png","pdf")){
  
  
  if(plot.which == "png"){ yrs.vec <- 2016:2018}  
  if(plot.which == "pdf"){ yrs.vec <- sort(unique(tags.merged.details$Year))}  
  
  
  if(plot.which == "pdf"){ pdf(file="DATA/ReportSupplement/TagData_Summary_PropRecoveryedComm_ByRelDate.pdf",
                               onefile=TRUE,height=8.5,width=11)}    
  
  
  
  
  for(plot.yr in yrs.vec){
    
  print(plot.yr)
  
  
if(plot.which == "png"){  png(filename = paste0("Data/ReportFigs/TagData_Summary_PropRecoveryedComm_ByRelDate_ForRMD_",plot.yr,".png"),
      width = 480*4, height = 480*3.7, units = "px", pointsize = 14*2.8, bg = "white",  res = NA) 
    }
  
  
  par(mfrow=c(2,1))
  
  
  dates.plot <- seq(as.Date(paste0(plot.yr,"-06-01")),as.Date(paste0(plot.yr,"-09-30")),by="day")
  yr.idx <- tags.merged.details$Year == plot.yr
  
  
  # plot releases
  rel.vec <- table(tags.merged.details$ReleaseDate[yr.idx])
  rel.dates <- as.Date(names(rel.vec))
  plot(dates.plot,rep(1,length(dates.plot)), type="n",xlab="Date",ylab="Tagged Fish",bty="n",ylim=c(0,max(10,rel.vec,na.rm=TRUE)))
  add.statweeks(dates.plot)
  rect(rel.dates-0.5,0,rel.dates+0.5,rel.vec,col="darkgrey",border="darkgrey")
  title(main=paste("Number Released & Number Recovered",plot.yr))
  
  
  #plot recoveries
  rec.vec <- table(tags.merged.details$ReleaseDate[yr.idx & comm.rec.idx  ])
  
  
  if(length(rec.vec)>0){
    rec.dates <- as.Date(names(rec.vec))
    rect(rec.dates-0.5,0,rec.dates+0.5,rec.vec,col="darkblue",border="darkblue")
  }
  
  
  merged.df <- as.data.frame(cbind(TagsOut = rel.vec, NumRecComm = 0, PercRecComm = 0))
  merged.df$NumRecComm[match(names(rec.vec),names(rel.vec))] <- rec.vec
  merged.df$PercRecComm <- round(merged.df$NumRec/merged.df$TagsOut*100)
  
  prop.rec.byday <- rbind(prop.rec.byday,merged.df)
  
  
  
  plot(dates.plot,rep(1,length(dates.plot)), type="n",xlab="Date",ylab="% Tags Recovered (Comm)",bty="n",ylim=c(0,100))
  add.statweeks(dates.plot)
  abline(h=0,col="darkgrey")
  lines(as.Date(dimnames(merged.df)[[1]]),type="o",merged.df$PercRecComm,pch=19,col="darkblue",cex=0.6)
  title(main=paste("Percent Recovered",plot.yr))
  
  
  
if(plot.which == "png"){dev.off()} # close png
  
  
} # end looping through years

if(plot.which == "pdf"){  dev.off()} # close pdf

} # end plot.which


# delete blank row
prop.rec.byday <- prop.rec.byday[-1,]


# write to file
write.csv(prop.rec.byday,"DATA/DerivedData/TagData_Summary_PropRecByReleaseDate.csv")




# ---------------------------------------------------------------------------------------------------
# REPORT FIGURE 15 and handout: Proportion of Tags Recovered in Cdn Commercial Fishery by Weekday of Release 

yrs.vec <- sort(unique(tags.merged.details$Year))
days.vec <- c("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
days.vec.short <- c("Sun","Mon","Tue","Wed","Thu","Fri","Sat")

comm.rec.idx <- !is.na(tags.merged.details$RecoveryDate) & tags.merged.details$RecoveryType=="Commercial"
comm.rec.idx[is.na(comm.rec.idx)] <- FALSE

###
# do it with loops....

yearday.combos <- expand.grid(yrs.vec,days.vec) 
yearday.combos <- yearday.combos[order(yearday.combos$Var1),]


comm.rec.idx <- !is.na(tags.merged.details$RecoveryDate) & tags.merged.details$RecoveryType=="Commercial"
comm.rec.idx[is.na(comm.rec.idx)] <- FALSE


proprec.summary <- data.frame(Year = yearday.combos$Var1, RelWeekday = yearday.combos$Var2, RelWeekdayShort = substr(yearday.combos$Var2,1,3)  ,
                              TagsOut=NA, TagsRecComm= NA,PropRecComm = NA)


for(yr.use in yrs.vec){
  for(day.use in days.vec){
    
    print("----------")
    print(yr.use)
    print(day.use)
    
    idx.summary <- proprec.summary$Year == yr.use & proprec.summary$RelWeekday == day.use
    idx.year <- tags.merged.details$Year == yr.use
    idx.day <- tags.merged.details$ReleaseWeekDay== day.use
    vec.use <-  tags.merged.details$RecoveryType[idx.year & idx.day]
    
    proprec.summary[idx.summary, "TagsOut"] <- length(vec.use)
    proprec.summary[idx.summary, "TagsRecComm"] <- sum(vec.use=="Commercial",na.rm=TRUE)
    
  }} # end looping through days and years


proprec.summary$PropRecComm <- round(proprec.summary$TagsRecComm /proprec.summary$TagsOut ,2)

proprec.summary

write.csv(proprec.summary,file="DATA/DerivedData/TagData_Summary_PropRecByWeekDay.csv",row.names=FALSE)


# do the plots

for(plot.which in c("png","pdf")){
  
  
  if(plot.which == "png"){ yrs.vec <- 2016:2018}  
  if(plot.which == "pdf"){ yrs.vec <- sort(unique(tags.merged.details$Year))}  
  
  
  if(plot.which == "pdf"){ pdf(file="DATA/ReportSupplement/TagData_WeekdaySummary_PropRecovery.pdf",
                               onefile=TRUE,height=8.5,width=11)
    
    par(mfrow=c(2,2))
    }    
  
  if(plot.which == "png"){ png(filename = "DATA/ReportFigs/TagData_WeekdaySummary_PropRecovery_ForRMD.png",
                               width = 480*4, height = 480*4.5, units = "px", pointsize = 14*2.8, bg = "white",  res = NA)
    par(mfrow=c(3,1),mai=c(2,5,1,5))
  } 
  
  
  for(plot.yr in yrs.vec){

  print(plot.yr )
  
  tmp.df  <- proprec.summary[proprec.summary$Year == plot.yr ,]
  plot(1:7, tmp.df$PropRecComm,type="n",bty="n",axes=FALSE, xlab="Release Weekday", ylab="Prop Recovered (Cdn Comm)",ylim=c(0,0.5),xlim=c(0,8))
  rect((1:7)-0.3, 0  ,(1:7)+0.3, tmp.df$PropRecComm,col="lightgrey",border="darkgrey")
  text(1:7,0,labels=tmp.df$TagsOut   ,xpd=NA,col="red",adj=c(0.5,1.1),cex=0.8)
  text(0,0,labels="TagsOut",xpd=NA,col="red",adj=c(0.5,1.1),cex=0.8)
  
  axis(2)
  axis(1,at=1:7,labels=days.vec.short)
  title(main = plot.yr )
  
  
} # end looping through years
  
if(plot.which == "png"){dev.off()} # close png
if(plot.which == "pdf"){  dev.off()} # close pdf

  } # end plot.which
  






# ---------------------------------------------------------------------------------------------------
# REPORT FIGURE 16 and handout: Time to Recovery in Cdn Commercial Fishery by Weekday of Release

yrs.vec <- sort(unique(tags.merged.details$Year))
days.vec <- c("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
days.vec.short <- c("Sun","Mon","Tue","Wed","Thu","Fri","Sat")

comm.rec.idx <- !is.na(tags.merged.details$RecoveryDate) & tags.merged.details$RecoveryType=="Commercial"
comm.rec.idx[is.na(comm.rec.idx)] <- FALSE

summary.fun <- function(x){
  
  vec.out <- c(sum(!is.na(x)),mean(x,na.rm=TRUE),quantile(x,probs=c(0.1,0.25,0.5,0.75,0.9),na.rm=TRUE))
  return(vec.out)
  
}


if(FALSE){
  # giving object that's weird to handle/relabel
  traveltime.summary <- as.data.frame(aggregate(tags.merged.details$TravelTime[comm.rec.idx],
                                                by=list(Year=tags.merged.details$Year[comm.rec.idx],RelWeekday = tags.merged.details$ReleaseWeekDay[comm.rec.idx]),
                                                FUN=summary.fun))
  #names(traveltime.summary)<- c("Year","ReleaseWeekday","nObs","Mean",paste0("p",c(10,25,50,75,90)))
}


if(FALSE){
  # tricky to sort proper day sequence
  par(mfrow=c(3,2))
  
  for(yr.use in yrs.vec[1:10]){
    yr.idx <- tags.merged.details$Year == yr.use
    use.idx <- yr.idx & comm.rec.idx
    if(sum(use.idx)>0){
      boxplot(tags.merged.details$TravelTime[use.idx] ~ tags.merged.details$Year[use.idx]* tags.merged.details$ReleaseWeekDay[use.idx],
              ylim=c(0,15))
      title(main=yr.use)
    }}}





###
# do it with loops....

yearday.combos <- expand.grid(yrs.vec,days.vec) 
yearday.combos <- yearday.combos[order(yearday.combos$Var1),]


comm.rec.idx <- !is.na(tags.merged.details$RecoveryDate) & tags.merged.details$RecoveryType=="Commercial"
comm.rec.idx[is.na(comm.rec.idx)] <- FALSE


traveltime.summary <- data.frame(Year = yearday.combos$Var1, RelWeekday = yearday.combos$Var2, RelWeekdayShort = substr(yearday.combos$Var2,1,3)  ,
                                 nObs=NA, Mean= NA, p10 = NA, p25 = NA, p50 = NA, p75 = NA, p90 = NA,p100=NA)




for(yr.use in yrs.vec){
  for(day.use in days.vec){
    
    print("----------")
    print(yr.use)
    print(day.use)
    
    idx.summary <- traveltime.summary$Year == yr.use & traveltime.summary$RelWeekday == day.use
    idx.year <- tags.merged.details$Year == yr.use
    idx.day <- tags.merged.details$ReleaseWeekDay== day.use
    vec.use <-  tags.merged.details$TravelTime[idx.year & idx.day & comm.rec.idx]
    
    traveltime.summary[idx.summary, "nObs"] <- sum(!is.na(vec.use))
    traveltime.summary[idx.summary, "Mean"] <- round(mean(vec.use,na.rm=TRUE),2)
    traveltime.summary[idx.summary, c("p10","p25","p50","p75","p90","p100")] <- quantile(vec.use,probs=c(0.1,0.25,0.5,0.75,0.9,1),na.rm=TRUE)
    
    
    
  }} # end looping through days and years


traveltime.summary

write.csv(traveltime.summary,file="DATA/DerivedData/TagData_Summary_TravelTime.csv",row.names=FALSE)



# do the plots

for(plot.which in c("png","pdf")){
  
  
  if(plot.which == "png"){ yrs.vec <- 2016:2018}  
  if(plot.which == "pdf"){ yrs.vec <- sort(unique(tags.merged.details$Year))}  
  
  
  if(plot.which == "pdf"){ pdf(file="DATA/ReportSupplement/TagData_WeekdaySummary_TimeToRecovery.pd.pdf",
                               onefile=TRUE,height=8.5,width=11)
    
    par(mfrow=c(2,2))
  }    
  
  if(plot.which == "png"){ png(filename = "DATA/ReportFigs/TagData_WeekdaySummary_TimeToRecovery_ForRMD.png",
                               width = 480*4, height = 480*4.5, units = "px", pointsize = 14*2.8, bg = "white",  res = NA)
    par(mfrow=c(3,1),mai=c(2,5,1,5))
  } 
  
  
  for(plot.yr in yrs.vec){
    
    print(plot.yr )

    
    
    tmp.df  <- traveltime.summary[traveltime.summary$Year == plot.yr,]
    
    plot.do <- sum(tmp.df$nObs)>0
    
    if(plot.do){
      
      
      plot(1:7, tmp.df$Mean,type="n",bty="n",axes=FALSE, xlab="Release Weekday", ylab="Time to Recovery",col="red",lwd=2,
           ylim=c(0,max(tmp.df$p90)),xlim=c(0,8))
      
      
      abline(h=c(4,8),col="darkgrey",lty=2)
      segments(1:7,tmp.df$p10,1:7,tmp.df$p90,col="darkgrey")
      rect((1:7)-0.3, tmp.df$p25   ,(1:7)+0.3, tmp.df$p75,col="lightgrey",border="darkgrey")
      #lines(1:7, tmp.df$Mean,type="l",col="red",lwd=2)
      #legend("topleft", legend = c("Half of Obs","80% of Obs"),bty="n",
      #			pch=c(22,NA),lty=c(NA,1),bg=c("lightgrey","NA"),pt.cex=2)
      
      text(c(0.5,1:7),0.4,labels=c("n=",tmp.df$nObs)   ,xpd=NA,col="red",adj=c(0.5,1.1),cex=0.8)
    } # end if doing plot
    
    
    
    if(!plot.do){plot(1:7, 1:7,type="n",bty="n",axes=FALSE, xlab="Release Weekday", ylab="Time to Recovery (Cdn Comm)",col="red",lwd=2,
                      ylim=c(0,10),xlim=c(0,8))}
    
    axis(2)
    axis(1,at=1:7,labels=days.vec.short)
    title(main = plot.yr)
    
    
    
    
  } # end looping through years
  
  if(plot.which == "png"){dev.off()} # close png
  if(plot.which == "pdf"){  dev.off()} # close pdf
  
} # end plot.which




