---
output:
  pdf_document: default
  html_document: default
---
# Taku-Sockeye-Public

Public repository of data and code from the Taku River Sockeye Salmon Stock Assessment Program and Escapement Goal reviews which were conducted from 2018 to 2020 by the Taku Sockeye Working Group.

## Disclaimer

### Alaska Department of Fish & Game

*ADF&G retains intellectual property rights to data collected by or for ADF&G. Any dissemination of the data must credit ADF&G as the source, with a disclaimer that exonerates the department for errors or deficiencies in reproduction, subsequent analysis, or interpretation. The State of Alaska, Department of Fish and Game, makes no express or implied warranties (including warranties of merchantability and fitness) with respect to the accuracy, character, function, or capabilities of the data, services, or products or their appropriateness for any particular purpose. The Department of Fish and Game is not liable for any direct, incidental, indirect, special, compensatory, consequential or other damages suffered by the user or any other person or entity from the use of or failure of the data, services, or products even if the Department of Fish and Game has been advised of the possibility of such potential loss or damage. The entire risk as to the results of the use of the data, services, or products is assumed by the user. Data may be subject to periodic change without prior notification.*


### Fisheries and Oceans Canada

*DFO retains intellectual property rights to data collected by or for DFO. The Data is provided “as is”, and DFO excludes all representations, warranties, obligations, and liabilities, whether express or implied, to the maximum extent permitted by law.*

*DFO is not liable for any errors or omissions in the Data, and will not under any circumstances be liable for any direct, indirect, special, incidental, consequential, or other loss, injury or damage caused by its use or otherwise arising in connection with this Data. Data is subject to periodic change without prior notification.*


## Project Overview

The Transboundary Chapter of the Pacific Salmon Treaty (PST Chapter 1 Annex IV (3)(b)(i)(C)) commits the Parties to:

1) “…develop a joint technical report and submit it through the Parties’ respective review mechanisms with the aim of establishing a bilaterally approved maximum sustainable yield (MSY) goal for Taku River sockeye salmon prior to the 2020 fishing season”, and, that
2) “The Taku River sockeye salmon assessment program will be reviewed by two experts (one selected by each Party) in mark-recovery estimation techniques. The Parties shall instruct these experts to make a joint recommendation to the Parties concerning improvements to the existing program including how to address inherent mark-recovery assumptions with an aim to minimize potential bias prior to the 2020 fishing season”.

As directed by the Transboundary Panel under the Pacific Salmon Treaty, the Transboundary Technical Committee assembled a working group with representatives from Fisheries and Oceans Canada, Alaska Department of Fish and Game, and the Taku River Tlingit First Nation, and capture-recapture specialists from both Canada and the U.S. The two experts, required under the treaty, were identified as Robert Clark (ADF&G - retired) and Dr. Carl Schwarz (SFU – retired).

The full Taku Sockeye Working Group consisted of:
* Julie Bednarski - ADF&G – Fisheries Biologist - Co-chair
* Aaron Foos – DFO – Sr. Aquatic Science Biologist - Co-chair
* Robert Clark – retired ADF&G – Consulting Fisheries Scientist
* Dr. Carl Schwarz – retired SFU – Consulting Biometrician
* Ian Boyce – DFO – Sr. Aquatic Science Biologist
* Dr. Sara Miller – ADF&G – Biometrician
* Dr. Paul Vecsei – DFO – Sr. Aquatic Science Biologist
* Dr. Rich Brenner – ADF&G – Salmon Stock Assessment Biologist
* Richard Erhardt – Taku River Tlingit First Nation – Consulting Biologist
* Gottfried Pestal – DFO – Consulting Biometrician
* Andrew Piston – ADF&G – Fisheries Biologist
* Philip Richards – ADF&G – Fisheries Biologist


## Project Outcomes

Between 2018 and 2020, the Taku Sockeye Working Group completed a comprehensive review of the stock assessment program for Taku River Sockeye Salmon and developed biological benchmarks based on reviewed and revised data, which are documented in two reports and various supplementary materials as per the following:

* *[Literature review](https://gitlab.com/transboundary-committee/Taku-Sockeye-Public/-/tree/master/LitReview)*: extensive literature review of primary literature and agency publications focused on capture–recapture studies, dropout estimates, and other Sockeye Salmon assessment approaches from other regions. Two versions of this reference library are available here. This includes pdf versions of most reports, abstracts, notes on relevance, and links to the original source where available.
* *Data review*: review of stock assessment data and analyses, review of capture-recapture estimation approaches and assumptions, bias corrections, investigations of alternatives, and updated abundance estimates ([Pestal et al., 2020](https://www.psc.org/download/33/psc-technical-reports/12486/psc-technical-report-no-43.pdf)). 
* *Supplementary materials*: additional information, diagnostic plots, and data files relevant to the data review (Pestal et al., 2020).
* *Biological benchmarks and escapement goal recommendations*: developed with a Bayesian state-space model using the updated abundance estimates ([Terms of Reference](https://www.dfo-mpo.gc.ca/csas-sccs/Schedule-Horraire/2019/11_05-06-eng.html), [summary of science advice](http://www.dfo-mpo.gc.ca/csas-sccs/Publications/SAR-AS/2020/2020_006-eng.html), and [Miller and Pestal, 2020](https://www.dfo-mpo.gc.ca/csas-sccs/Publications/ResDocs-DocRech/2020/2020_035-eng.html)). 

In addition, many of the decisions and recommendations from the working group process related to improvements in stock assessment are reflected
reflected in the annual operational plan for the Taku River Sockeye Salmon Assessment program as of 2019 [(Bednarksi et al., 2019)](http://www.adfg.alaska.gov/FedAidPDFs/ROP.CF.1J.2019.10.pdf).








